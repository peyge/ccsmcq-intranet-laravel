<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsPendingToReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if (! Schema::hasColumn('reports', 'is_pending'))
        {
            Schema::table('reports', function (Blueprint $table) {
                $table->boolean('is_pending')->default('1');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('reports', 'is_pending'))
        {
            Schema::table('reports', function (Blueprint $table) {
                $table->dropColumn('is_pending');
            });
        }
    }
}
