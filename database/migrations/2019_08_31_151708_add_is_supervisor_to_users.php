<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsSupervisorToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        if (! Schema::hasColumn('users', 'is_supervisor'))
        {
            Schema::table('users', function (Blueprint $table) {
                $table->integer('is_supervisor')->default('0');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('users', 'is_supervisor'))
        {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('is_supervisor');
            });
        }
    }
}
