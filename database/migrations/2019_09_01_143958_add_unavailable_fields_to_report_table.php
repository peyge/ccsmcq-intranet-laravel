<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnavailableFieldsToReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasColumn('reports', 'date_from'))
        {
            Schema::table('reports', function (Blueprint $table) {
                $table->dateTime('date_from')->nullable();
            });
        }

        if (! Schema::hasColumn('reports', 'date_to'))
        {
            Schema::table('reports', function (Blueprint $table) {
                $table->dateTime('date_to')->nullable();
            });
        }

        if (! Schema::hasColumn('reports', 'additional_truck_to_replace'))
        {
            Schema::table('reports', function (Blueprint $table) {
                $table->boolean('additional_truck_to_replace')->default(0);
            });
        }

        if (! Schema::hasColumn('reports', 'additional_truck_number'))
        {
            Schema::table('reports', function (Blueprint $table) {
                $table->string('additional_truck_number')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('reports', 'date_from'))
        {
            Schema::table('reports', function (Blueprint $table) {
                $table->dropColumn('date_from');
            });
        }

        if (Schema::hasColumn('reports', 'date_to'))
        {
            Schema::table('reports', function (Blueprint $table) {
                $table->dropColumn('date_to');
            });
        }

        if (Schema::hasColumn('reports', 'additional_truck_to_replace'))
        {
            Schema::table('reports', function (Blueprint $table) {
                $table->dropColumn('additional_truck_to_replace');
            });
        }
        
        if (Schema::hasColumn('reports', 'additional_truck_number'))
        {
            Schema::table('reports', function (Blueprint $table) {
                $table->dropColumn('additional_truck_number');
            });
        }
    }
}
