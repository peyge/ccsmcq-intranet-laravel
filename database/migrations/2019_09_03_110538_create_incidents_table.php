<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::create('partners', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('linked_table')->nullable();
            $table->boolean('active')->default(0);
            $table->timestamps();
        });

        Schema::create('complaint_types', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('active')->default(0);
            $table->timestamps();
        });

        Schema::create('shifts', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('active')->default(0);
            $table->timestamps();
        });

        Schema::create('workstations', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('active')->default(0);
            $table->timestamps();
        });

        Schema::create('frequencies', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('active')->default(0);
            $table->timestamps();
        });

        Schema::create('incident_results', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('active')->default(0);
            $table->timestamps();
        });

        Schema::create('incidents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->unsignedInteger('involved_partner_id');
            $table->unsignedInteger('involved_partner_linked_id')->nullable();
            $table->string('involved_partner_details')->nullable();
            $table->unsignedInteger('main_complaint_type_id');
            $table->string('main_complaint_details')->nullable();
            $table->unsignedInteger('shift_id');
            $table->unsignedInteger('workstation_id');
            $table->unsignedInteger('frequency_id')->nullable();
            $table->string('card_number')->nullable();
            $table->dateTime('incident_date');
            $table->string('description');
            $table->string('answer')->nullable();
            $table->string('tap_1')->nullable();
            $table->string('tap_2')->nullable();
            $table->unsignedInteger('incident_result_id')->nullable();
            $table->boolean('claim_is_founded')->nullable();
            $table->unsignedInteger('from_rmu_user_id');
            $table->date('date_sent');
            $table->unsignedInteger('treated_by_user_id')->nullable();
            $table->date('date_treated')->nullable();
            $table->timestamps();

            $table->foreign('involved_partner_id')->references('id')->on('partners');
            $table->foreign('main_complaint_type_id')->references('id')->on('complaint_types');
            $table->foreign('shift_id')->references('id')->on('shifts');
            $table->foreign('workstation_id')->references('id')->on('workstations');
            $table->foreign('frequency_id')->references('id')->on('frequencies');
            $table->foreign('incident_result_id')->references('id')->on('incident_results');
            $table->foreign('from_rmu_user_id')->references('id')->on('users');
            $table->foreign('treated_by_user_id')->references('id')->on('users');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners');
        Schema::dropIfExists('complaint_types');
        Schema::dropIfExists('shifts');
        Schema::dropIfExists('workstations');
        Schema::dropIfExists('frequencies');
        Schema::dropIfExists('incident_results');
        Schema::dropIfExists('incidents');
    }
}
