<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLinkToAlerts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasColumn('alerts', 'link'))
        {
            Schema::table('alerts', function (Blueprint $table) {
                $table->string('link')->default('');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('alerts', 'link'))
        {
            Schema::table('alerts', function (Blueprint $table) {
                $table->dropColumn('link');
            });
        }
    }
}
