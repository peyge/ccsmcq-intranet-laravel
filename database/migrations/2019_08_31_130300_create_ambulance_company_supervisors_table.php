<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmbulanceCompanySupervisorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ambulance_company_supervisors', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ambulance_company_id');
            $table->string('name');
            $table->timestamps();
            $table->boolean('active')->default(0);

            $table->foreign('ambulance_company_id')->references('id')->on('ambulance_companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ambulance_company_supervisors');
    }
}
