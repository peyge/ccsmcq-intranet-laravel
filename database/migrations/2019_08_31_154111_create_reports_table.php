<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->dateTime('add1084')->nullable();
            $table->dateTime('add1086')->nullable();
            $table->dateTime('add1088')->nullable();
            $table->dateTime('add1089')->nullable();
            $table->dateTime('start1084real')->nullable();
            $table->dateTime('start1084planned')->nullable();
            $table->dateTime('end1089real')->nullable();
            $table->dateTime('end1089planned')->nullable();
            $table->boolean('unfulfilled')->default('0');
            $table->string('unfulfilled_reason')->nullable();
            $table->date('request_date');
            $table->time('request_time');
            $table->date('add_date')->nullable();
            $table->string('truck_number')->nullable();
            $table->unsignedInteger('ambulance_company_id');
            $table->unsignedInteger('ambulance_company_supervisor_id')->nullable();
            $table->string('ambulance_company_supervisor_other')->nullable();
            $table->string('reason_other')->nullable();
            $table->unsignedInteger('ccs_supervisor_user_id');
            $table->unsignedInteger('from_rmu_user_id');
            $table->string('notes')->nullable();
            $table->timestamps();

            $table->foreign('ambulance_company_id')->references('id')->on('ambulance_companies');
            $table->foreign('ambulance_company_supervisor_id')->references('id')->on('ambulance_company_supervisors');
            $table->foreign('ccs_supervisor_user_id')->references('id')->on('users');
            $table->foreign('from_rmu_user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_reasons');
    }
}
