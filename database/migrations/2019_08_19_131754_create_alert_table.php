<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlertTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alerts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('message');
            $table->timestamps();
            $table->unsignedInteger('created_by_user_id');
            $table->unsignedInteger('for_user_id');
            $table->boolean('is_read')->default(0);
            $table->timestamp('read_date')->nullable();

            $table->foreign('created_by_user_id')->references('id')->on('users');
            $table->foreign('for_user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alerts');
    }
}
