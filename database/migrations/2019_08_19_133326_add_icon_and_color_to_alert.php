<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIconAndColorToAlert extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if (! Schema::hasColumn('alerts', 'color'))
        {
            Schema::table('alerts', function (Blueprint $table) {
                $table->string('color')->default('primary');
            });
        }

        if (! Schema::hasColumn('alerts', 'icon'))
        {
            Schema::table('alerts', function (Blueprint $table) {
                $table->string('icon')->default('bullhorn');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('alerts', 'color'))
        {
            Schema::table('alerts', function (Blueprint $table) {
                $table->dropColumn('color');
            });
        }

        if (Schema::hasColumn('alerts', 'icon'))
        {
            Schema::table('alerts', function (Blueprint $table) {
                $table->dropColumn('icon');
            });
        }
    }
}
