<?php

use Illuminate\Database\Seeder;

class AmbulanceCompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ambulance_companies')->insert([
            'name' => 'CAM',
            'active' => '1'
        ]);

        DB::table('ambulance_companies')->insert([
            'name' => 'Ambulances Drummondville (Dessercom)',
            'active' => '1'
        ]);

        DB::table('ambulance_companies')->insert([
            'name' => 'Ambulances Louiseville (Dessercom)',
            'active' => '1'
        ]);

        DB::table('ambulance_companies')->insert([
            'name' => 'Ambulances Manseau (Dessercom)',
            'active' => '1'
        ]);

        DB::table('ambulance_companies')->insert([
            'name' => 'Ambulances Pierreville (Dessercom)',
            'active' => '1'
        ]);

        DB::table('ambulance_companies')->insert([
            'name' => 'Ambulance 22-22',
            'active' => '1'
        ]);

        DB::table('ambulance_companies')->insert([
            'name' => 'Ambulances Lyster (Dessercom)',
            'active' => '1'
        ]);

        DB::table('ambulance_companies')->insert([
            'name' => 'Ambulances Plessisville (Dessercom)',
            'active' => '1'
        ]);

        DB::table('ambulance_companies')->insert([
            'name' => 'Ambulance SAMU',
            'active' => '1'
        ]);

        DB::table('ambulance_companies')->insert([
            'name' => 'Ambulance St-Amand',
            'active' => '1'
        ]);
        DB::table('ambulance_companies')->insert([
            'name' => 'Ambulance 33-33 inc.',
            'active' => '1'
        ]);

        DB::table('ambulance_companies')->insert([
            'name' => 'BTAQ',
            'active' => '1'
        ]);

        DB::table('ambulance_companies')->insert([
            'name' => 'Urgence Bois-Franc inc.',
            'active' => '1'
        ]);
    }
}
