<?php

use Illuminate\Database\Seeder;

class IncidentsLinkTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('partners')->insert([
            'name' => 'Centre primaire',
            'linked_table' => 'primary_centers',
            'active' => '1'
        ]);

        DB::table('partners')->insert([
            'name' => 'Entreprise ambulancière',
            'linked_table' => 'ambulance_companies',
            'active' => '1'
        ]);

        DB::table('partners')->insert([
            'name' => 'Interne (CCS)',
            'linked_table' => 'users',
            'active' => '1'
        ]);

        DB::table('partners')->insert([
            'name' => 'Service incendie',
            'linked_table' => '',
            'active' => '1'
        ]);

        DB::table('partners')->insert([
            'name' => 'Service policier',
            'linked_table' => '',
            'active' => '1'
        ]);

        DB::table('partners')->insert([
            'name' => 'Service PR',
            'linked_table' => 'first_responders',
            'active' => '1'
        ]);

        DB::table('partners')->insert([
            'name' => 'Autre (spécifiez)',
            'linked_table' => '',
            'active' => '1'
        ]);

        DB::table('complaint_types')->insert([
            'name' => 'Propos inappropriés',
            'active' => '1'
        ]);

        DB::table('complaint_types')->insert([
            'name' => 'Refus de procéder',
            'active' => '1'
        ]);

        DB::table('complaint_types')->insert([
            'name' => 'Auto-répartition',
            'active' => '1'
        ]);

        DB::table('complaint_types')->insert([
            'name' => 'N\'a pas respecté les directives du CCS',
            'active' => '1'
        ]);

        DB::table('complaint_types')->insert([
            'name' => 'Délai d\'affectation',
            'active' => '1'
        ]);

        DB::table('complaint_types')->insert([
            'name' => 'Erreur de validation de coordonnées',
            'active' => '1'
        ]);

        DB::table('complaint_types')->insert([
            'name' => 'Appel transféré au mauvais service',
            'active' => '1'
        ]);

        DB::table('complaint_types')->insert([
            'name' => 'Autre (spécifiez)',
            'active' => '1'
        ]);

        DB::table('shifts')->insert([
            'name' => '00h00 - 08h00',
            'active' => '1'
        ]);

        DB::table('shifts')->insert([
            'name' => '06h00 - 14h00',
            'active' => '1'
        ]);

        DB::table('shifts')->insert([
            'name' => '06h00 - 18h00',
            'active' => '1'
        ]);

        DB::table('shifts')->insert([
            'name' => '08h00 - 16h00',
            'active' => '1'
        ]);

        DB::table('shifts')->insert([
            'name' => '14h00 - 22h00',
            'active' => '1'
        ]);

        DB::table('shifts')->insert([
            'name' => '16h00 - 00h00',
            'active' => '1'
        ]);

        DB::table('shifts')->insert([
            'name' => '18h00 - 06h00',
            'active' => '1'
        ]);

        DB::table('shifts')->insert([
            'name' => '22h00 - 06h00',
            'active' => '1'
        ]);

        DB::table('shifts')->insert([
            'name' => 'Autre (Jour)',
            'active' => '1'
        ]);

        DB::table('shifts')->insert([
            'name' => 'Autre (Soir)',
            'active' => '1'
        ]);

        DB::table('shifts')->insert([
            'name' => 'Autre (Nuit)',
            'active' => '1'
        ]);

        DB::table('workstations')->insert([
            'name' => 'Poste 1 (2201 - VD)',
            'active' => '1'
        ]);

        DB::table('workstations')->insert([
            'name' => 'Poste 2 (2202 - Chef)',
            'active' => '1'
        ]);

        DB::table('workstations')->insert([
            'name' => 'Poste 3 (2203 - T)',
            'active' => '1'
        ]);

        DB::table('workstations')->insert([
            'name' => 'Poste 4 (2204 - Surplus)',
            'active' => '1'
        ]);

        DB::table('workstations')->insert([
            'name' => 'Poste 5 (2205 - G)',
            'active' => '1'
        ]);

        DB::table('workstations')->insert([
            'name' => 'Poste 6 (2206 - Surplus)',
            'active' => '1'
        ]);

        DB::table('workstations')->insert([
            'name' => 'Poste 7 (2207 - TA/TB)',
            'active' => '1'
        ]);

        DB::table('workstations')->insert([
            'name' => 'Poste 8 (2208 - T1/T2)',
            'active' => '1'
        ]);

        DB::table('workstations')->insert([
            'name' => 'Autre',
            'active' => '1'
        ]);

        DB::table('frequencies')->insert([
            'name' => 'MCQOPER1 - Trois-Rivières',
            'active' => '1'
        ]);

        DB::table('frequencies')->insert([
            'name' => 'MCQOPER2',
            'active' => '1'
        ]);

        DB::table('frequencies')->insert([
            'name' => 'MCQOPER3 - Grand-Mère',
            'active' => '1'
        ]);

        DB::table('frequencies')->insert([
            'name' => 'MCQOPER4',
            'active' => '1'
        ]);

        DB::table('frequencies')->insert([
            'name' => 'MCQOPER5 - Victoriaville',
            'active' => '1'
        ]);

        DB::table('frequencies')->insert([
            'name' => 'MCQOPER6',
            'active' => '1'
        ]);

        DB::table('frequencies')->insert([
            'name' => 'MCQEVEN7 - Drummondville',
            'active' => '1'
        ]);

        DB::table('frequencies')->insert([
            'name' => 'MCQEVEN8',
            'active' => '1'
        ]);

        DB::table('frequencies')->insert([
            'name' => 'MCQEVEN9',
            'active' => '1'
        ]);

        DB::table('frequencies')->insert([
            'name' => 'INTERCCS',
            'active' => '1'
        ]);

        DB::table('frequencies')->insert([
            'name' => 'MCQURG07',
            'active' => '1'
        ]);

        DB::table('frequencies')->insert([
            'name' => 'PR Victo',
            'active' => '1'
        ]);

        DB::table('incident_results')->insert([
            'name' => 'Rapport envoyé à l\'entreprise',
            'active' => '1'
        ]);

        DB::table('incident_results')->insert([
            'name' => 'Rapport envoyé au partenaire',
            'active' => '1'
        ]);

        DB::table('incident_results')->insert([
            'name' => 'Rapport envoyé à l\'interne',
            'active' => '1'
        ]);

        DB::table('incident_results')->insert([
            'name' => 'RMU rencontré (Par CE en poste)',
            'active' => '1'
        ]);

        DB::table('incident_results')->insert([
            'name' => 'Aucun (Rien à faire)',
            'active' => '1'
        ]);

        DB::table('incident_results')->insert([
            'name' => 'Aucun (déjà traîté)',
            'active' => '1'
        ]);

        DB::table('incident_results')->insert([
            'name' => 'Rapport compilé',
            'active' => '1'
        ]);

        DB::table('incident_results')->insert([
            'name' => 'Inconnu',
            'active' => '1'
        ]);
    }
}
