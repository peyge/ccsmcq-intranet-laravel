<?php

use Illuminate\Database\Seeder;

class AddResourcesReasonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('reasons')->insert([
            'type' => 'addResource',
            'name' => 'Activité spéciale',
            'active' => '1'
        ]);

         DB::table('reasons')->insert([
            'type' => 'addResource',
            'name' => 'Conditions climatiques',
            'active' => '1'
        ]);

         DB::table('reasons')->insert([
            'type' => 'addResource',
            'name' => 'Couverture incendie',
            'active' => '1'
        ]);

         DB::table('reasons')->insert([
            'type' => 'addResource',
            'name' => 'Couverture policière',
            'active' => '1'
        ]);

         DB::table('reasons')->insert([
            'type' => 'addResource',
            'name' => 'Débordement d\'appels',
            'active' => '1'
        ]);

         DB::table('reasons')->insert([
            'type' => 'addResource',
            'name' => 'Détournement CH',
            'active' => '1'
        ]);

         DB::table('reasons')->insert([
            'type' => 'addResource',
            'name' => 'P-4/P-7 en attente depuis plus de 3h',
            'active' => '1'
        ]);

         DB::table('reasons')->insert([
            'type' => 'addResource',
            'name' => 'Transfert hors zone',
            'active' => '1'
        ]);
    }
}
