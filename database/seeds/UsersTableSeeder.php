<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Administrator',
            'email' => 'operations@ccsmcq.org',
            'password' => bcrypt('ccsmcq'),
            'is_admin' => '1',
            'image' => ''
        ]);

        DB::table('users')->insert([
            'name' => 'Patrice Guillemette',
            'email' => 'patrice.guillemette@ccsmcq.org',
            'password' => bcrypt('ccsmcq'),
            'is_admin' => '1',
            'image' => ''
        ]);

        DB::table('users')->insert([
            'name' => 'Simple User',
            'email' => 'user@ccsmcq.org',
            'password' => bcrypt('ccsmcq'),
            'is_admin' => '0',
            'image' => ''
        ]);
    }
}
