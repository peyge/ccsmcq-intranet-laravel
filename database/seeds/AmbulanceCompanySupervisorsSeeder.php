<?php

use Illuminate\Database\Seeder;

class AmbulanceCompanySupervisorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Mario Doucet',
            'ambulance_company_id' => '1',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Raphael Bélanger-Castonguay',
            'ambulance_company_id' => '1',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Stéphan Juteau',
            'ambulance_company_id' => '1',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Alexandre Hébert',
            'ambulance_company_id' => '13',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Joël Fortier',
            'ambulance_company_id' => '13',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Léo Bonin',
            'ambulance_company_id' => '2',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Christian Dion',
            'ambulance_company_id' => '2',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'François Lamothe',
            'ambulance_company_id' => '2',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Patrick Giguère',
            'ambulance_company_id' => '2',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Marie-Claude Richard',
            'ambulance_company_id' => '6',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Serge Richard',
            'ambulance_company_id' => '6',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Miguel Leblanc',
            'ambulance_company_id' => '6',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Bruno Gauthier',
            'ambulance_company_id' => '12',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Denis Tremblay',
            'ambulance_company_id' => '12',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Jimmy Lessard',
            'ambulance_company_id' => '12',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Sylvain Gauthier',
            'ambulance_company_id' => '12',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Érick Hovington',
            'ambulance_company_id' => '3',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Dany Abbott',
            'ambulance_company_id' => '3',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Alain Ayotte',
            'ambulance_company_id' => '4',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Chantale Laforce',
            'ambulance_company_id' => '5',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Camille Houle-Courchesne',
            'ambulance_company_id' => '5',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Érick Hovington',
            'ambulance_company_id' => '5',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Claude Gagnon',
            'ambulance_company_id' => '9',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Steve Delisle',
            'ambulance_company_id' => '11',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Carl Picard',
            'ambulance_company_id' => '11',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Bruno Gauthier',
            'ambulance_company_id' => '11',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Jimmy Lessard',
            'ambulance_company_id' => '11',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Denis Tremblay',
            'ambulance_company_id' => '11',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Sylvain Gauthier',
            'ambulance_company_id' => '11',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Chantale St-Amand',
            'ambulance_company_id' => '10',
            'active' => '1'
        ]);

        DB::table('ambulance_company_supervisors')->insert([
            'name' => 'Julie St-Amand',
            'ambulance_company_id' => '10',
            'active' => '1'
        ]);
    }
}
