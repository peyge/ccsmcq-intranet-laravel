<?php

use Illuminate\Database\Seeder;

class UnavailableResourcesReasonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reasons')->insert([
            'type' => 'unavailableResource',
            'name' => 'Bris mécanique (majeur)',
            'active' => '1'
        ]);

        DB::table('reasons')->insert([
            'type' => 'unavailableResource',
            'name' => 'Débordement CNESST',
            'active' => '1'
        ]);
        DB::table('reasons')->insert([
            'type' => 'unavailableResource',
            'name' => 'Désinfection prolongée (majeure)',
            'active' => '1'
        ]);

        DB::table('reasons')->insert([
            'type' => 'unavailableResource',
            'name' => 'TA/P souillé (majeur)',
            'active' => '1'
        ]);

        DB::table('reasons')->insert([
            'type' => 'unavailableResource',
            'name' => 'Installation / Entretien RENIR',
            'active' => '1'
        ]);

        DB::table('reasons')->insert([
            'type' => 'unavailableResource',
            'name' => 'Manque un TA/P',
            'active' => '1'
        ]);

        DB::table('reasons')->insert([
            'type' => 'unavailableResource',
            'name' => 'Équipement manquant/défectueux (majeur)',
            'active' => '1'
        ]);

        DB::table('reasons')->insert([
            'type' => 'partiallyUnavailableResource',
            'name' => 'Bris mécanique (mineur)',
            'active' => '1'
        ]);

        DB::table('reasons')->insert([
            'type' => 'partiallyUnavailableResource',
            'name' => 'Équipement manquant/défectueux (mineur)',
            'active' => '1'
        ]);

        DB::table('reasons')->insert([
            'type' => 'partiallyUnavailableResource',
            'name' => 'Rencontre administrative',
            'active' => '1'
        ]);

        DB::table('reasons')->insert([
            'type' => 'partiallyUnavailableResource',
            'name' => 'TA/P souillé (mineur)',
            'active' => '1'
        ]);

        DB::table('reasons')->insert([
            'type' => 'partiallyUnavailableResource',
            'name' => 'Désinfection prolongée (mineur)',
            'active' => '1'
        ]);
    }
}
