<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ambulance_company extends Model
{
    protected $table = 'ambulance_companies';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'name', 'active'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ambulance_company_supervisors()
    {
        return $this->hasMany('App\Ambulance_company_supervisors', 'ambulance_company_id', 'id');
    }

}
