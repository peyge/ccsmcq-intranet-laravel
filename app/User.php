<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_admin', 'image', 'active', 'theme'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'active' => 'boolean',
    ];

    /**
     * Hash et affecte le nouveau mot de passe
     * @param $password
     */
    public function hashAndSetPassword($password)
    {
        $this->password = \Hash::make($password);
    }

    /**
     * Fill the model with an array of attributes.
     *
     * @param  array  $attributes
     * @return $this
     *
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     */
    public function fill(array $attributes)
    {
        //$attributes['password'] = isset($attributes['password']) && $attributes['password'] ?
        //    \Hash::make($attributes['password']) : null;
        if (!isset($attributes['password'])) {
            unset($attributes['password']);
        }

        return parent::fill($attributes);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function alerts()
    {
        return $this->hasMany('App\Alert', 'for_user_id', 'id');
    }

    public function theme()
    {

        return $this->theme;
        /*return [
            'monokai ' => [
                'backgrounds' => [
                        '272822',
                        '3e3d32',
                        '75715e'
                    ],
                'texts' => [
                        'f8f8f2'
                    ],
                'colors' => [
                    'yellow' => 'e6db74', 
                    'orange' => 'fd971f',
                    'red' => 'f92672',
                    'magenta' => 'fd5ff0',
                    'violet' => 'ae81ff', 
                    'blue' => '66d9ef', 
                    'cyan' => 'a1efe4',
                    'green' => 'a6e22e'
                ]
            ],
            'default' => [

            ]
        ];*/
    }
}
