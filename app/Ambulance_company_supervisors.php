<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ambulance_company_supervisors extends Model
{

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'name', 'ambulance_company_id', 'active'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ambulance_company()
    {
        return $this->belongsTo('App\Ambulance_company', 'id', 'ambulance_company_id');
    }

}
