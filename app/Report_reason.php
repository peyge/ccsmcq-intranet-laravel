<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report_reason extends Model
{

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'report_id', 'reason_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function report()
    {
        return $this->belongsTo('App\Report', 'id', 'report_id');
    }

}
