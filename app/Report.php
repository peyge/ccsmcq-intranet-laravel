<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'type',
         'add1084',
         'add1086',
         'add1088',
         'add1089',
         'start1084real',
         'start1084planned',
         'end1089real',
         'end1089planned',
         'unfulfilled',
         'unfulfilled_reason',
         'request_date',
         'request_time',
         'add_date',
         'truck_number',
         'ambulance_company_id',
         'ambulance_company_supervisor_id',
         'ambulance_company_supervisor_other',
         'reason_other',
         'ccs_supervisor_user_id',
         'from_rmu_user_id',
         'notes',
         'date_from',
         'date_to',
         'additional_truck_to_replace',
         'additional_truck_number',
    ];

    public function type_to_name()
    {
        switch($this->type)
        {
            case 'add' : 
                $result = "Ajout de ressources";
                break;
            case 'start' : 
                $result = "Début hâtif";
                break;
            case 'end' : 
                $result = "Fin tardive";
                break;
            case 'full_unavailable' : 
                $result = "10-0-6";
                break;
            case 'partially_unavailable' : 
                $result = "10-25";
                break;
            default : 
                $result = "Inconnu";
        }

        return $result;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cards()
    {
        return $this->hasMany('App\Report_card', 'report_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reasons()
    {
        return $this->hasMany('App\Report_reason', 'report_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ambulance_company()
    {
        return $this->hasOne('App\Ambulance_company', 'id', 'ambulance_company_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ambulance_company_supervisor()
    {
        return $this->hasOne('App\Ambulance_company_supervisors', 'id', 'ambulance_company_supervisor_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function from_rmu()
    {
        return $this->hasOne('App\User', 'id', 'from_rmu_user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ccs_supervisor()
    {
        return $this->hasOne('App\User', 'id', 'ccs_supervisor_user_id');
    }
}
