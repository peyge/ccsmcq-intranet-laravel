<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Alert;
use App\Report;
use App\Ambulance_company;
use App\Ambulance_company_supervisors;
use App\Reason;
use App\User;
use App\Report_card;
use App\Report_reason;

class OperationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listAddResources()
    {
        activity()
           ->causedBy(\Auth::user())
           ->log('Ouverture de la liste d\'ajout de ressources.');

        $reports = Report::where('is_pending', '1')->whereIn('type', ['add', 'start', 'end'])->get();

        return view('operations.reports.listAddRessources', [
            'reports' => $reports
        ]);
    }

    public function addResources(Report $report)
    {
        activity()
           ->causedBy(\Auth::user())
           ->log('Ouverture du formulaire d\'ajout de ressources.');

        $ambulance_companies = Ambulance_company::where('active', '1')->orderBy('name', 'ASC')->get();
        $ambulance_company_supervisors = Ambulance_company_supervisors::where('active', '1')->orderBy('name', 'ASC')->get();
        $reasons = Reason::where('type', 'addResource')->where('active', '1')->orderBy('name', 'ASC')->get();
        $ccs_supervisors = User::where('active', '1')->where('is_supervisor', '1')->orderBy('name', 'ASC')->get();
        $users = User::where('active', '1')->orderBy('name', 'ASC')->get();

        if($report->id)
        {
            if($report->ambulance_company_supervisor_other != '')
            {
                $report->ambulance_company_supervisor_id = '-1';
            }

            if($report->reason_other != '')
            {
                $report->reason_other_chkbx = '-1';
            }
        } else {
            $report->is_pending = 1;
        }

        return view('operations.reports.addRessources', [
            'report' => $report,
            'ambulance_companies' => $ambulance_companies,
            'ambulance_company_supervisors' => $ambulance_company_supervisors,
            'reasons' => $reasons,
            'ccs_supervisors' => $ccs_supervisors,
            'users' => $users
        ]);
    }

    public function saveResources(Request $request, Report $report)
    {
        activity()
           ->causedBy(\Auth::user())
           ->log('Enregistrement d\'un'.($report->id == '' ? ' nouvel' : 'e modification à un').' ajout de ressources.');

        $report->fill($request->all());

        $report->unfulfilled = '0';

        if($request->has('unfulfilled'))
        {
            $report->unfulfilled = '1';
        }
        else
        {
            $report->unfulfilled_reason = null;
        }

        if($request->get('ambulance_company_supervisor_id') == '-1')
        {
            $report->ambulance_company_supervisor_id = null;
        }
        else
        {
            $report->ambulance_company_supervisor_other = '';
        }

        if(! $request->has('reason_other_chkbx'))
        {
            $report->reason_other = '';
        }

        if($request->has('is_pending'))
        {
            $report->is_pending = $request->get('is_pending');
        }

        $report->save();

        // manage the cards
        $posted_cards = [];
        if($request->has('card'))
        {
            foreach($request->get('card') as $k => $card)
            {
                //First, check if the card already exists to make sure not to duplicate it.
                if($card != '' && $report->cards()->where('card_number', $card)->get()->count() == 0)
                {
                    //If the card doesn't alreasy exists, create it
                    $report_card = new Report_card();
                    $report_card->report_id = $report->id;
                    $report_card->card_number = $card;
                    $report_card->save();
                }
                if($card != '')
                {
                    $posted_cards[] = $card;
                }
            }
        }

        //Delete the cards that are not present anymore
        foreach($report->cards as $card)
        {
            if(! in_array($card->card_number, $posted_cards))
            {
                $card->delete();
            }
        }

        //Manage the reasons
        $posted_reasons = [];
        if($request->has('reason'))
        {
            foreach($request->get('reason') as $k => $reason_id)
            {
                //First, check if the reason already exists to make sure not to duplicate it.
                if($report->reasons()->where('reason_id', $reason_id)->get()->count() == 0)
                {
                    //If the reason doesn't alreasy exists, create it
                    $report_reason = new Report_Reason();
                    $report_reason->report_id = $report->id;
                    $report_reason->reason_id = $reason_id;
                    $report_reason->save();
                }
                $posted_reasons[] = $reason_id;
            }
        }

        //Delete the reasons that are not present anymore
        foreach($report->reasons as $reason)
        {
            if(! in_array($reason->reason_id, $posted_reasons))
            {
                $reason->delete();
            }
        }

        if($report->is_pending)
        {
            $request->session()->flash('alert-success', 'Le rapport "'.$report->type_to_name().'" est enregistré en tant que brouillon avec succès.');
        }
        else
        {
            //Send the report to the CEs
            \Ccsmcq::alertChefEquipe([
                'title' => 'Nouveau rapport "'.$report->type_to_name().'".',
                'message' => 'Un nouveau rapport appelé "'.$report->type_to_name().'" a été envoyé par '.\Auth::user()->name.'.',
                'color' => 'info',
                'icon' => 'file',
                'link' => route('operations.report.ressources', ['report' => $report->id]),
            ]);

            $request->session()->flash('alert-success', 'Le rapport "'.$report->type_to_name().'" est envoyé au chef d\'équipe.');
        }


        return redirect()->route('operations.report.ressources.list');
    }













    public function listUnavailableResources()
    {
        activity()
           ->causedBy(\Auth::user())
           ->log('Ouverture de la liste des non-disponibilités des ressources ambulancières.');

        $reports = Report::where('is_pending', '1')->whereIn('type', ['full_unavailable', 'partially_unavailable'])->get();

        return view('operations.reports.listUnavailableRessources', [
            'reports' => $reports
        ]);
    }

    public function unavailableResources(Report $report)
    {
        activity()
           ->causedBy(\Auth::user())
           ->log('Ouverture du formulaire de non-disponibilité des ressources ambulancières.');

        $ambulance_companies = Ambulance_company::where('active', '1')->orderBy('name', 'ASC')->get();
        $ambulance_company_supervisors = Ambulance_company_supervisors::where('active', '1')->orderBy('name', 'ASC')->get();
        $reasons = Reason::where('type', 'unavailableResource')->where('active', '1')->orderBy('name', 'ASC')->get();
        $reasons_partially = Reason::where('type', 'partiallyUnavailableResource')->where('active', '1')->orderBy('name', 'ASC')->get();
        $ccs_supervisors = User::where('active', '1')->where('is_supervisor', '1')->orderBy('name', 'ASC')->get();
        $users = User::where('active', '1')->orderBy('name', 'ASC')->get();

        if($report->id)
        {
            if($report->ambulance_company_supervisor_other != '')
            {
                $report->ambulance_company_supervisor_id = '-1';
            }

            if($report->reason_other != '')
            {
                $report->reason_other_chkbx = '-1';
            }
        } else {
            $report->is_pending = 1;
        }

        return view('operations.reports.addUnavailableRessources', [
            'report' => $report,
            'ambulance_companies' => $ambulance_companies,
            'ambulance_company_supervisors' => $ambulance_company_supervisors,
            'reasons' => $reasons,
            'reasons_partially' => $reasons_partially,
            'ccs_supervisors' => $ccs_supervisors,
            'users' => $users
        ]);
    }

    public function saveUnavailableResources(Request $request, Report $report)
    {
        activity()
           ->causedBy(\Auth::user())
           ->log('Enregistrement d\'une'.($report->id == '' ? ' nouvelle' : ' modification à une').' non-disponibilité de ressource ambulancière.');

        $report->fill($request->all());

        $report->unfulfilled = '0';
        $report->unfulfilled_reason = null;

        $report->request_date = date('Y-m-d');
        $report->request_time = date('H:i');

        if($request->get('ambulance_company_supervisor_id') == '-1')
        {
            $report->ambulance_company_supervisor_id = null;
        }
        else
        {
            $report->ambulance_company_supervisor_other = '';
        }

        if(! $request->has('reason_other_chkbx'))
        {
            $report->reason_other = '';
        }

        if($request->has('is_pending'))
        {
            $report->is_pending = $request->get('is_pending');
        }

        if($request->has('additional_truck_to_replace') && $request->get('additional_truck_to_replace') == '0')
        {
            $report->additional_truck_number = '';
        }



        $report->save();

        //Manage the reasons
        $posted_reasons = [];
        if($request->has('reason'))
        {
            foreach($request->get('reason') as $k => $reason_id)
            {
                //First, check if the reason already exists to make sure not to duplicate it.
                if($report->reasons()->where('reason_id', $reason_id)->get()->count() == 0)
                {
                    //If the reason doesn't alreasy exists, create it
                    $report_reason = new Report_Reason();
                    $report_reason->report_id = $report->id;
                    $report_reason->reason_id = $reason_id;
                    $report_reason->save();
                }
                $posted_reasons[] = $reason_id;
            }
        }

        //Delete the reasons that are not present anymore
        foreach($report->reasons as $reason)
        {
            if(! in_array($reason->reason_id, $posted_reasons))
            {
                $reason->delete();
            }
        }

        if($report->is_pending)
        {
            $request->session()->flash('alert-success', 'Le rapport "'.$report->type_to_name().'" est enregistré en tant que brouillon avec succès.');
        }
        else
        {
            $request->session()->flash('alert-success', 'Le rapport "'.$report->type_to_name().'" est envoyé au chef d\'équipe.');
        }


        return redirect()->route('operations.report.unavailableRessources.list');
    }
}
