<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the reports home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        activity()
           ->causedBy(\Auth::user())
           ->log('User in report home');
        return view('home');
    }

    public function ajoutRessources()
    {
        
    }
}
