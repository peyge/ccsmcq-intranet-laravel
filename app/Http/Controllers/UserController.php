<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ProfilRequest;
use app\User;
use Spatie\Activitylog\Models\Activity;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the reports home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function profil()
    {
        $user = \Auth::user();

        activity()
           ->causedBy($user)
           ->log('Ouverture de la page d\'édition du profil.');

        return view('user.profil', ['user' => $user]);
    }

    public function saveProfil(ProfilRequest $request)
    {
        $user = \Auth::user();
        $user->fill($request->all());
        if($request->has('password'))
        {
            $user->hashAndSetPassword($request->get('password'));
        }

        if($request->hasFile('image'))
        {
            $big_image = \Image::make(\Input::file('image')->getRealPath())->resize(60, 60);

            $user->image = $big_image->encode('data-url');
        }

        activity()
           ->causedBy($user)
           ->performedOn($user)
           ->log('Sauvegarde du profil effectuée.');

        $user->save();

        $request->session()->flash('alert-success', 'Votre profil à été sauvegardé avec succès.');

        return redirect()->route('user.profil');
    }

    public function activity(Request $request)
    {
        $user = \Auth::user();
        //->causedBy($user)
   
        \Carbon::setLocale('fr');

        $activities = Activity::where('causer_id', $user->id)->where('causer_type', 'App\User')->orderBy('id', 'DESC')->get();

        return view('user.activity', ['user' => $user, 'activities' => $activities]);
    }

}
