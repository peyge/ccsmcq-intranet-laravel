<?php

namespace App\Http\Controllers;

use App\Report;
use App\User;
use App\Reason;
use Illuminate\Http\Request;
use PDF;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function addResources($isPending = 0, $dateFrom=null, $dateTo=null)
    {
        activity()
           ->causedBy(\Auth::user())
           ->log('Ouverture de la liste administrative d\'ajout de ressources.');

        $reports = Report::where('is_pending', $isPending)->whereIn('type', ['add', 'start', 'end'])->get();

        return view('admin.reports.listAddRessources', [
            'reports' => $reports
        ]);
    }

    public function addResourcesGeneratePdf(Report $report)
    {
        activity()
           ->causedBy(\Auth::user())
           ->log('Impression PDF d\'un ajout de ressources.');

        $reasons = Reason::where('type', 'addResource')->where('active', '1')->orderBy('name', 'ASC')->get();

        view()->share('report',$report);
        view()->share('reasons',$reasons);

        //return view('admin.reports.printAddRessources');

        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        // pass view file
        $pdf = PDF::loadView('admin.reports.printAddRessources');
        // download pdf
        return $pdf->download('Ajout de ressources.pdf');
        //return $pdf->stream();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function listUnavailableResources($isPending = 0, $dateFrom=null, $dateTo=null)
    {
        activity()
           ->causedBy(\Auth::user())
           ->log('Ouverture de la liste administrative des non-disponibilités.');

        $reports = Report::where('is_pending', $isPending)->whereIn('type', ['full_unavailable', 'partially_unavailable'])->get();

        return view('admin.reports.listUnavailableRessources', [
            'reports' => $reports
        ]);
    }

    public function unavailableResourcesGeneratePdf(Report $report)
    {
        activity()
           ->causedBy(\Auth::user())
           ->log('Impression PDF d\'une non-disponibilité.');

           if($report->type=='partially_unavailable')
           {
                $reasons = Reason::where('type', 'partiallyUnavailableResource')->where('active', '1')->orderBy('name', 'ASC')->get();
           }
           else
           {
                $reasons = Reason::where('type', 'unavailableResource')->where('active', '1')->orderBy('name', 'ASC')->get();
           }

        

        view()->share('report',$report);
        view()->share('reasons',$reasons);

        //return view('admin.reports.printUnavailableRessources');

        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        // pass view file
        $pdf = PDF::loadView('admin.reports.printUnavailableRessources');
        // download pdf
        return $pdf->download('Non-disponibilité des ressources ambulancières.pdf');
        //return $pdf->stream();
    }

    public function listUsers(Request $request)
    {
        activity()
           ->causedBy(\Auth::user())
           ->log('Ouverture de la liste des utilisateurs.');

        $users = User::get();

        return view('admin.users.list', [
            'users' => $users
        ]);
    }
}