<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

/**
 * Classe LocationRequest
 */
class ProfilRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @param Request $request
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $constraints = [];
        $constraints['email'] = 'required|email';
        $constraints['name'] = 'required';

        return $constraints;
    }

    /**
     * Définit les messages d'erreur pour la validation d'un usager lors
     * de sa création et modification.
     * @return array
     */
    public function messages()
    {
        $messages['email.required'] = 'Le courriel est requis.';
        $messages['email.email'] = 'Le courriel doit être une adresse courriel valide.';
        $messages['name.required'] = 'Le nom est requis.';

        return $messages;
    }
}
