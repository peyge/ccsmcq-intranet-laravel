<?php // Code within app\Helpers\Ccsmcq.php

namespace App\Helpers;

use App\Alert;
use App\User;

class ccsmcq
{
    public static function alertChefEquipe($params = [])
    {
        $chefs = User::where('active', '1')->where('is_supervisor', '1')->orderBy('name', 'ASC')->get();

        foreach($chefs as $chef)
        {
        	self::alertUser($chef->id, $params);
        }
    }

    public static function alertUser($user_id, $params =[])
    {
        $alert = new Alert();
        $alert->title = (isset($params['title']) ? $params['title'] : 'Nouveau message');
        $alert->message = (isset($params['message']) ? $params['message'] : 'Nouveau message'); 
        $alert->created_by_user_id = \Auth::user()->id;
        $alert->for_user_id = $user_id;
        $alert->is_read = '0';
        $alert->color = (isset($params['color']) ? $params['color'] : 'info');
        $alert->icon = (isset($params['icon']) ? $params['icon'] : 'comment');
        $alert->link = (isset($params['link']) ? $params['link'] : '#');
        $alert->save();
    }
}