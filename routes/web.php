<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/',function () {
    return view('home');
});*/
Route::get('/', 'HomeController@index')->name('home');
//Auth::routes();

/**
 * Login Route(s)
 */
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
/**
 * Register Route(s)
 */
//Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//Route::post('register', 'Auth\RegisterController@register');

/**
 * Password Reset Route(S)
 */
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

/**
 * Email Verification Route(s)
 */
Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');


Route::group(['middleware' => 'auth', 'prefix' => 'user'], function () {
    Route::get('profil', 'UserController@profil')->name('user.profil');
    Route::post('profil', 'UserController@saveProfil')->name('user.profil.save');
    Route::get('activity', 'UserController@activity')->name('user.activity');
});

Route::group(['middleware' => 'auth', 'prefix' => 'operations'], function () {
    Route::get('report/ressources/list/add', 'OperationController@listAddResources')->name('operations.report.ressources.list');
    Route::get('report/ressources/add/{report?}', 'OperationController@addResources')->name('operations.report.ressources');
    Route::post('report/ressources/add/{report?}', 'OperationController@saveResources')->name('operations.report.ressources.save');

    Route::get('report/ressources/list/unavailable', 'OperationController@listUnavailableResources')->name('operations.report.unavailableRessources.list');
    Route::get('report/ressources/unavailable/{report?}', 'OperationController@unavailableResources')->name('operations.report.unavailableRessources');
    Route::post('report/ressources/unavailable/{report?}', 'OperationController@saveUnavailableResources')->name('operations.report.unavailableRessources.save');
});

Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function () {
    Route::get('report/ressources/list/add', 'AdminController@addResources')->name('admin.report.ressources.list');
    Route::get('report/ressources/print/add/{report}', 'AdminController@addResourcesGeneratePdf')->name('admin.report.ressources.print');
    //Route::get('report/ressources/add/{report?}', 'OperationController@addResources')->name('operations.report.ressources');
    //Route::post('report/ressources/add/{report?}', 'OperationController@saveResources')->name('operations.report.ressources.save');

    Route::get('report/ressources/list/unavailable', 'AdminController@listUnavailableResources')->name('admin.report.unavailableRessources.list');
    Route::get('report/ressources/print/unavailable/{report}', 'AdminController@unavailableResourcesGeneratePdf')->name('admin.report.unavailableRessources.print');
    //Route::get('report/ressources/unavailable/{report?}', 'OperationController@unavailableResources')->name('operations.report.unavailableRessources');
    //Route::post('report/ressources/unavailable/{report?}', 'OperationController@saveUnavailableResources')->name('operations.report.unavailableRessources.save');


    Route::get('users', 'AdminController@listUsers')->name('admin.users.list');
    Route::get('users/add', 'AdminController@addUsers')->name('admin.users.add');
    Route::get('users/edit/{user}', 'AdminController@addUsers')->name('admin.users.edit');
});






Route::get('/home', 'HomeController@index')->name('home');
