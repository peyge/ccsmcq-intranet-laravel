<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>{{ config('app.name', 'Laravel') }}@isset($title) | {{ $title }}@endisset</title>

  <!-- Custom fonts for this template-->
  <link href="{{ asset('/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="{{ URL::asset('css/sb-admin-2.min.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">


  @if(\Auth::user()->theme() == 'monokai')
    <style>
      .bg-gradient-primary{
        background: #272822 !important;
      }
      .bg-white{
        background: #3e3d32 !important;
      }

      .dropdown-menu{
        background: #3e3d32 !important;
        color: #f8f8f2;
      }

      .modal-body{
        background: #3e3d32 !important;
        color: #f8f8f2;
      }

      .modal-header{
        background: #272822 !important;
        color: #f8f8f2;
      }

      .modal-footer{
        background: #272822 !important;
        color: #f8f8f2;
      }

      .dropdown-header{
        background: #272822 !important;
      }

      .dropdown-item, .collapse-item{
        background: #3e3d32 !important;
        color: #f8f8f2 !important;
      }

      .dropdown-item:hover, .collapse-item:hover{
        background: #75715e !important;
        color: #f8f8f2 !important;
      }

      #content-wrapper{
        background: #75715e !important;
        color: #f8f8f2 !important;
      }

      .card, .card-body{
        background: #918f81 !important;
        color: #f8f8f2 !important;
      }

      .card-body2{
        background: #272822 !important;
        color: #f8f8f2 !important;
      }

      .card-header{
        background: #3e3d32 !important;
      }

      .dataTable{
        color: #f8f8f2 !important;
      }

      input, select, textarea{
        background: #3e3d32 !important;
        color: #f8f8f2 !important;
      }

      h1, h2, h3, h4, h5, h6, .text-gray-800{
        color: #fd971f !important;
      }

      .btn-warning{
        background-color: #e6db74 !important;
      }

       .btn-info{
        background-color: #66d9ef !important;
      }

      .alert-success{
        background-color: #a6e22e !important;
      }

      .alert-danger{
        background-color: #f92672 !important;
      }

      .alert-warning{
        background-color: #e6db74 !important;
      }

      .alert-info{
        background-color: #66d9ef !important;
      }

    </style>
  @endif

<?php 

//Base icons for the header
  $iconsArr = [
    'clinic-medical', 
    'coffee', 
    'first-aid', 
    'headset', 
    'laptop-medical'];

    //halloween icons 28-31 octobre each years
    if(date('m') == '10' && (date('d') >= 28 && date('d') <=31 ))
    {
      $iconsArr = [
        'book-dead',
        'cat',
        'crow',
        'ghost',
        'hat-wizard',
        'skull-crossbones',
        'spider'
      ];
    }

    //Christmas icons 20-30 decembre each years
    if(date('m') == '12' && (date('d') >= 20 && date('d') <=30 ))
    {
      $iconsArr = [
        'candy-cane',
        'gift',
        'gifts',
        'holly-berry',
        'sleigh',
        'snowman',
      ];
    }

    //New year icons 31 dec-2 jan each years
    if(date('m-d') == '12-31' || ( (str_pad(date('m'), 2, '0', STR_PAD_LEFT) == '01') && date('d') >= 1 && date('d') <=2 ) )
    {
      $iconsArr = [
        'glass-cheers',
        'gift',
        'glass-cheers',
        'gifts',
        'glass-cheers',
        'holly-berry',
        'glass-cheers',
      ];
    }

    //Valentine day icons 12-16 february each years
    if(str_pad(date('m'), 2, '0', STR_PAD_LEFT) == '02' && (date('d') >= 12 && date('d') <=16 ))
    {
      $iconsArr = [
        'heart',
        'grin-hearts',
        'kiss-wink-heart',
      ];
    }
?>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-<?php echo $iconsArr[array_rand($iconsArr)]; ?>"></i>
        </div>
        <div class="sidebar-brand-text mx-3">{{ config('app.name', 'Laravel')}}</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="/">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Tableau de bord</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Opérations
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-clipboard"></i>
          <span>Rapports</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Équipes:</h6>
            <a class="collapse-item" href="{{route('operations.report.ressources.list')}}"><i class="fas fa-fw fa-user-plus"></i>&nbsp;Ajouts de ressources</a>
            <a class="collapse-item" href="{{route('operations.report.unavailableRessources.list')}}"><i class="fas fa-fw fa-user-minus"></i>&nbsp;Non disponibilités</a>
            <h6 class="collapse-header">Interne:</h6>
            <a class="collapse-item" href="buttons.html"><i class="fas fa-fw fa-bullhorn"></i>&nbsp;Communication</a>
            <a class="collapse-item" href="buttons.html"><i class="fas fa-fw fa-user-injured"></i>&nbsp;Rapport d'incident</a>
            <a class="collapse-item" href="buttons.html"><i class="fas fa-fw fa-baby"></i>&nbsp;RMU en probation</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="tables.html">
          <i class="fas fa-fw fa-comment"></i>
          <span>Notes de services</span></a>
      </li>


      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Administration
      </div>

      <!-- Nav Item - Charts -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-clipboard"></i>
          <span>Rapports</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Rapports:</h6>
            <a class="collapse-item" href="{{route('admin.report.unavailableRessources.list')}}"><i class="fas fa-fw fa-user-minus"></i>&nbsp;10-25/10-0-6</a>
            <a class="collapse-item" href="{{route('admin.report.ressources.list')}}"><i class="fas fa-fw fa-user-plus"></i>&nbsp;Ajouts de ressources</a>
            <a class="collapse-item" href="utilities-other.html"><i class="fas fa-fw fa-user-injured"></i>&nbsp;Incident</a>
            <a class="collapse-item" href="utilities-other.html"><i class="fas fa-fw fa-ambulance"></i>&nbsp;Rétroactions terrain</a>
          </div>
        </div>
      </li>
      
      <li class="nav-item">
        <a class="nav-link" href="tables.html">
          <i class="fas fa-fw fa-ambulance"></i>
          <span>Entrée de rétroactions</span></a>
      </li>

      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="{{route('admin.users.list')}}">
          <i class="fas fa-fw fa-users"></i>
          <span>Utilisateurs</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>


          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Alerts -->
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <!-- Counter - Alerts -->
                <span class="badge badge-danger badge-counter">{{count(\Auth::user()->alerts)}}</span>
              </a>
              <!-- Dropdown - Alerts -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                  Alertes
                </h6>
                @foreach(\Auth::user()->alerts as $alert)
                  <a class="dropdown-item d-flex align-items-center" href="{{$alert->link or '#'}}">
                    <div class="mr-3">
                      <div class="icon-circle bg-{{$alert->color}}">
                        <i class="fas fa-{{$alert->icon}} text-white"></i>
                      </div>
                    </div>
                    <div><?php Carbon::setLocale('fr_FR'); setlocale(LC_TIME, 'fr_FR');?>
                      <div class="small text-gray-500">Publié le {{Carbon::parse($alert->created_at)->formatLocalized('%d %B %Y')}}</div>
                      <strong>{{$alert->title}}</strong><br />
                      {{$alert->message}}
                    </div>
                  </a>
                @endforeach
                @if( count(\Auth::user()->alerts) == 0)
                  <a class="dropdown-item d-flex align-items-center" href="#">
                    <div class="mr-3">
                      <div class="icon-circle bg-primary">
                        <i class="fas fa-info text-white"></i>
                      </div>
                    </div>
                    <div>
                      <div class="small text-gray-500">Publié le {{Carbon::parse(date('Y-m-d H:i:s'))->formatLocalized('%d %B %Y')}}</div>
                      <strong>Aucune alerte</strong><br />
                      Il n'y a aucune alerte pour l'instant.
                    </div>
                  </a>
                @endif
              </div>
            </li>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ Auth::user()->name }}</span>
                <img class="img-profile rounded-circle" src="@if(\Auth::user()->image) {{ \Auth::user()->image  }} @else /user.png @endif">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="{{ route('user.profil') }}">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profil
                </a>
                <!--<a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Paramètres
                </a>-->
                <a class="dropdown-item" href="{{ route('user.activity') }}">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Mon activité
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" onclick="event.preventDefault();" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Déconnexion
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
<div class="flash-message col-md-12 col-md-offset-3">
   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
          <p class="alert alert-{{ $msg }} bold" style="position: relative; z-index: 9999">{!! Session::get('alert-' . $msg) !!} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
  @endforeach
</div>
        @yield('content')

      </div>
      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; <a href="http://patriceguillemette.com" target="_blank">Patrice Guillemette</a> {{date('Y')}}</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Prêt à quitter?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Sélectionnez "Déconnexion" ci-dessous si vous êtes prêt à terminer la session en cours.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Annuler</button>
          <a class="btn btn-primary" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Déconnexion</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="{{ asset('/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{ asset('/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ asset('/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ asset('/js/sb-admin-2.min.js')}}"></script>

  <!-- Page level plugins -->
  <script src="{{ asset('/vendor/chart.js/Chart.min.js')}}"></script>
  <script src="{{ asset('/vendor/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{ asset('/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
  <script src="{{ asset('/js/bootstrap-datetimepicker.min.js')}}"></script>


  <script>
    // Call the dataTables jQuery plugin
    $(document).ready(function() {
      $('.dataTable').DataTable({
        "ordering": false, 
    language: {
        processing:     "Traitement en cours...",
        search:         "Rechercher&nbsp;:",
        lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
        info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
        infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
        infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
        infoPostFix:    "",
        loadingRecords: "Chargement en cours...",
        zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
        emptyTable:     "Aucune donnée disponible dans le tableau",
        paginate: {
            first:      "Premier",
            previous:   "Pr&eacute;c&eacute;dent",
            next:       "Suivant",
            last:       "Dernier"
        },
        aria: {
            sortAscending:  ": activer pour trier la colonne par ordre croissant",
            sortDescending: ": activer pour trier la colonne par ordre décroissant"
        }
    }
});
    });


    
</script>

  <!-- Page level custom scripts -->
  @stack('scripts')
  

</body>

</html>
