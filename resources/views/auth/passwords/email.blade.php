@extends('layouts.login')

@section('content')

 <div class="container">
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-password-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-2">Vous avez oublié votre mot de passe?</h1>
                    <p class="mb-4">Nous savons que ce problème arrive souvent. Vous n'avez qu'à entrer votre adresse courriel ci-dessous et nous vous enverrons un lien pour changer votre mot de passe!</p>
                  </div>
                  <form class="user" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="email" class="form-control form-control-user" id="email" name="email" aria-describedby="emailHelp" value="{{ old('email') }}" required placeholder="Entrez votre courriel...">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                      
                    </div>
                    <button type="submit" href="login.html" class="btn btn-primary btn-user btn-block">
                      Changer mon mot de passe
                    </button>
                  </form>
                  <hr>
                  <div class="text-center">
                    <a class="small" href="{{ route('home') }}">Vous vous rappelez de votre mot de passe? Se connecter!</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
@endsection
