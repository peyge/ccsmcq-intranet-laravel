<!DOCTYPE html>
<html>
    <head>
        <title>Non-disponibilité des ressources ambulancières</title>
        <link href="{{ asset('/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link href="{{ URL::asset('css/sb-admin-2.min.css') }}" rel="stylesheet">
        <style>
            *{color: black;font-size: 10pt;}
            .important{font-weight: bold;}
        </style>
        <?php Carbon::setLocale('fr_FR'); setlocale(LC_TIME, 'fr_FR');?>
    </head>
    <body>
        <div class="container-fluid">
            <img src="http://ccsmcq.org/bundles/extendfrontendcore/images/logos/ccsmcq.gif" width="180" />
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h4>Non-disponibilité des ressources ambulancières</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h4 style="background-color: grey; color: black; font-weight: bold; text-align:center;">{{$report->type_to_name()}}</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table cellspacing="0" border="1" style="width:100%;">
                        <tr>
                            <td>Date</td>
                            <td>
                                {{Carbon::parse($report->date_from)->formatLocalized('%d %B %Y')}}
                                @if(Carbon::parse($report->date_from)->formatLocalized('%d %B %Y') != Carbon::parse($report->date_to)->formatLocalized('%d %B %Y'))
                                    - {{Carbon::parse($report->date_to)->formatLocalized('%d %B %Y')}}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Heure de la demande</td>
                            <td>
                                De: {{Carbon::parse($report->date_from)->formatLocalized('%Hh%M')}}
                                à: {{Carbon::parse($report->date_to)->formatLocalized('%Hh%M')}}
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                @foreach($reasons as $reason)
                                    @if($report->reasons()->where('reason_id', $reason->id)->get()->count() == 0)
                                        <i class="far fa-square"></i>
                                    @else
                                        <i class="far fa-check-square"></i>
                                    @endif
                                    {{$reason->name}}<br />
                                @endforeach
                                Autre (spécifiez): {{$report->reason_other}}
                            </td>
                        </tr>
                        <tr>
                            <td>Entreprise ambulancière: </td>
                            <td>
                                @if($report->ambulance_company_id)
                                    {{$report->ambulance_company->name}}
                                @endif
                            </td>
                        </tr>
                        @if($report->type == 'full_unavailable')
                            <tr>
                                <td>Nom du responsable d'entreprise avisé: </td>
                                <td>
                                    @if($report->ambulance_company_supervisor_id)
                                        {{$report->ambulance_company_supervisor->name}}
                                    @else
                                        {{$report->ambulance_company_supervisor_other}}
                                    @endif
                                </td>
                            </tr>
                        @endif
                        <tr>
                            <td>N° de VA: </td>
                            <td>{{$report->truck_number}}</td>
                        </tr>

                        @if($report->type == 'full_unavailable')
                            <tr>
                                <td colspan="2">
                                    <p>Y a-t-il une équipe additionnelle pour remplacer la RA non disponible ?</p>
                                    @if($report->additional_truck_to_replace == '1')
                                        <i class="far fa-check-square"></i>
                                    @else
                                        <i class="far fa-square"></i>
                                    @endif
                                    Oui &nbsp;&nbsp;N° de VA: {{$report->additional_truck_number}}<br />
                                    @if($report->additional_truck_to_replace == '0')
                                        <i class="far fa-check-square"></i>
                                    @else
                                        <i class="far fa-square"></i>
                                    @endif
                                    Non
                                </td>
                            </tr>
                        @endif
                        @if($report->type == 'full_unavailable' && $report->ccs_supervisor_user_id)
                            <tr>
                                <td colspan="2">
                                    <span class="important">Nom du responsable du CCS ayant donné l'autorisation</span><br />
                                    {{$report->ccs_supervisor->name}}
                                </td>
                            </tr>
                        @endif
                        <tr>
                            <td colspan="2">
                                <span class="important">Nom et matricule du RMU</span><br />
                                {{$report->from_rmu->name}} - {{$report->from_rmu->number}}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>