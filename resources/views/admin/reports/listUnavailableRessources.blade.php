@extends('layouts.base', ['title' => 'Liste des non-disponibilités'])

@section('content')


<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Liste des non-disponibilités</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 style="display: inline;" class="m-0 font-weight-bold text-primary">Soumis</h6>
              <span style="float:right">
                <a href="{{route('operations.report.ressources')}}" class="btn btn-success btn-circle">
                  <i class="fas fa-plus"></i>
                </a>
              </span>
            </div>
            <div class="card-body">
              @if(count($reports) > 0)
                <div class="table-responsive">
                  <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                      <tr>
                        <th>Type</th>
                        <th>Date de début</th>
                        <th>Date de fin</th>
                        <th>Durée</th>
                        <th>Entreprise ambulancière</th>
                        <th>Débuté par</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th>Type</th>
                        <th>Date de début</th>
                        <th>Date de fin</th>
                        <th>Durée</th>
                        <th>Entreprise ambulancière</th>
                        <th>Débuté par</th>
                        <th>Actions</th>
                      </tr>
                    </tfoot>
                    <tbody>
                      @foreach($reports as $report)
                          <tr>
                            <td>{{$report->type_to_name()}}</td>
                            <?php Carbon::setLocale('fr_FR'); setlocale(LC_TIME, 'fr_FR');?>
                            <td>
                              @if($report->date_from)
                                {{Carbon::parse($report->date_from)->formatLocalized('%d %B %Y %H:%M')}}
                              @else
                                Inconnu
                              @endif
                            </td>
                            <td>
                              @if($report->date_to)
                                {{Carbon::parse($report->date_to)->formatLocalized('%d %B %Y %H:%M')}}
                              @else
                                Inconnu
                              @endif
                              </td>
                            <td>
                              @if($report->date_from && $report->date_to)
                                {{str_replace('second', 'seconde', str_replace('hour', 'heure', str_replace('day', 'journée', str_replace('month', 'mois', str_replace('year', 'année', Carbon::parse($report->date_from)->diffForHumans(Carbon::parse($report->date_to), true, false, 5))))))}}
                              @else
                                Rapport incomplet
                              @endif
                            </td>
                            <td>
                              @if($report->ambulance_company_id)
                                {{$report->ambulance_company->name}}
                              @else
                                Rapport incomplet
                              @endif
                            </td>
                            <td>{{$report->from_rmu->name}}</td>
                            <td>
                                <a href="{{route('operations.report.unavailableRessources', ['report' => $report->id])}}" class="btn btn-warning btn-circle">
                                    <i class="far fa-edit"></i>
                                </a>
                                <a href="{{route('admin.report.unavailableRessources.print', ['report' => $report->id])}}" class="btn btn-info btn-circle">
                                    <i class="far fa-file-pdf"></i>
                                </a>
                            </td>
                          </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              @else
                <p>Aucun rapport de non disponibilités pour la période choisie. Vous pouvez utiliser le bouton vert au haut de la page afin d'en créer un nouveau.</p>
              @endif
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

@endsection
