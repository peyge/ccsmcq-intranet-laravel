<!DOCTYPE html>
<html>
    <head>
        <title>Ajout de ressource, début hâtif et fin tardive</title>
        <link href="{{ asset('/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link href="{{ URL::asset('css/sb-admin-2.min.css') }}" rel="stylesheet">
        <style>
            *{color: black;font-size: 10pt;}
            .important{font-weight: bold;}
        </style>
        <?php Carbon::setLocale('fr_FR'); setlocale(LC_TIME, 'fr_FR');?>
    </head>
    <body>
        <div class="container-fluid">
            <img src="http://ccsmcq.org/bundles/extendfrontendcore/images/logos/ccsmcq.gif" width="180" />
            <div class="row">
                <div class="col-lg-12 center">
                    <h4 style="background-color: grey; color: black; font-weight: bold; text-align:center;">Ajout de ressource, début hâtif et fin tardive</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p style="margin: 0px; padding: 0px;">Inscrire les heures lorsque les TA/P s'annoncent. Si l'équipe ne s'est pas rapportée, inscrire ND pour non disponible.</p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <table cellspacing="0" border="1" style="width:100%;">
                        <tr>
                            <td class="important">
                                @if($report->type == 'add')
                                    <i class="far fa-check-square"></i>
                                @else
                                    <i class="far fa-square"></i>
                                @endif
                                &nbsp;&nbsp;
                                Ajout de ressources
                            </td>
                            <td>
                                <table style="width: 100%;">
                                    <tr>
                                        <td>10-84</td>
                                        <td>10-86</td>
                                        <td>10-88</td>
                                        <td>10-89</td>
                                    </tr>
                                    <tr>
                                        @if($report->type == 'add')
                                            <td>{{Carbon::parse($report->add1084)->formatLocalized('%H:%M')}}</td>
                                            <td>{{Carbon::parse($report->add1086)->formatLocalized('%H:%M')}}</td>
                                            <td>{{Carbon::parse($report->add1088)->formatLocalized('%H:%M')}}</td>
                                            <td>{{Carbon::parse($report->add1089)->formatLocalized('%H:%M')}}</td>
                                        @else
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        @endif
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="important">
                                @if($report->type == 'start')
                                    <i class="far fa-check-square"></i>
                                @else
                                    <i class="far fa-square"></i>
                                @endif
                                &nbsp;&nbsp;
                                Début hâtif
                            </td>
                            <td>
                                <table style="width: 100%;">
                                    <tr>
                                        <td>10-84 réel</td>
                                        <td>10-84 prévu à l'horaire</td>
                                    </tr>
                                    <tr>
                                        @if($report->type == 'start')
                                            <td>{{Carbon::parse($report->start1084real)->formatLocalized('%H:%M')}}</td>
                                            <td>{{Carbon::parse($report->start1084planned)->formatLocalized('%H:%M')}}</td>
                                        @else
                                            <td></td>
                                            <td></td>
                                        @endif
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="important">
                                @if($report->type == 'end')
                                    <i class="far fa-check-square"></i>
                                @else
                                    <i class="far fa-square"></i>
                                @endif
                                &nbsp;&nbsp;
                                Fin tardive
                            </td>
                            <td>
                                <table style="width: 100%;">
                                    <tr>
                                        <td>10-89 réel</td>
                                        <td>10-89 prévu à l'horaire</td>
                                    </tr>
                                    <tr>
                                        @if($report->type == 'end')
                                            <td>{{Carbon::parse($report->end1089real)->formatLocalized('%H:%M')}}</td>
                                            <td>{{Carbon::parse($report->end1089planned)->formatLocalized('%H:%M')}}</td>
                                        @else
                                            <td></td>
                                            <td></td>
                                        @endif
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="important">
                                @if($report->type == 'unfulfilled')
                                    <i class="far fa-check-square"></i>
                                @else
                                    <i class="far fa-square"></i>
                                @endif
                                &nbsp;&nbsp;
                                Non comblé
                            </td>
                            <td>
                                Raison:
                                @if($report->type == 'unfulfilled')
                                    {{$report->unfulfilled_reason}}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Date de la demande</td>
                            <td>{{Carbon::parse($report->request_date)->formatLocalized('%d %B %Y')}}</td>
                        </tr>
                        <tr>
                            <td>Heure de la demande</td>
                            <td>{{Carbon::parse($report->request_time)->formatLocalized('%Hh%M')}}</td>
                        </tr>
                        <tr>
                            <td>Date de l'ajout</td>
                            <td>{{Carbon::parse($report->add_date)->formatLocalized('%d %B %Y')}}</td>
                        </tr>
                        <tr>
                            <td>N° du VA</td>
                            <td>{{$report->truck_number}}</td>
                        </tr>
                        <tr>
                            <td>Entreprise ambulancière</td>
                            <td>{{$report->ambulance_company->name}}</td>
                        </tr>
                        <tr>
                            <td>Responsable de l'entreprise ambulancière</td>
                            <td>
                                @if($report->ambulance_company_supervisor_id)
                                    {{$report->ambulance_company_supervisor->name}}
                                @else
                                    {{$report->ambulance_company_supervisor_other}}
                                @endif
                            </td>
                        </tr>





                        <tr style="background-color:grey;">
                            <td class="important">N° carte d'appel</td>
                            <td class="important">Sélectionnez la raison</td>
                        </tr>
                        <tr>
                            <td>
                                @if($report->cards()->get()->count() > 0)
                                    &nbsp;&nbsp;{{$report->cards[0]->card_number}}
                                @endif
                            </td>
                            <td rowspan="8">
                                @foreach($reasons as $reason)
                                    @if($report->reasons()->where('reason_id', $reason->id)->get()->count() == 0)
                                        <i class="far fa-square"></i>
                                    @else
                                        <i class="far fa-check-square"></i>
                                    @endif
                                    {{$reason->name}}<br />
                                @endforeach
                                    Autre (spécifiez): {{$report->reason_other}}
                            </td>
                        </tr>
                        @for($i=1; $i<=7; $i++)
                            <tr>
                                <td>
                                    @if($report->cards()->get()->count() > $i)
                                        &nbsp;&nbsp;{{$report->cards[$i]->card_number}}
                                    @else
                                        &nbsp;
                                    @endif
                                </td>
                            </tr>
                        @endfor
                    </table>

                    <table style="width:100%; border:1px solid black;">
                        <tr>
                            <td>
                                <span class="important">Nom du responsable du CCS ayant donné l'autorisation</span><br />
                                {{$report->ccs_supervisor->name}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="important">Nom et matricule du RMU</span><br />
                                {{$report->from_rmu->name}} - {{$report->from_rmu->number}}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>