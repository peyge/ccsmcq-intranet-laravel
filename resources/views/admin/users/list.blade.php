@extends('layouts.base', ['title' => 'Liste des utilisateurs'])

@section('content')


<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Liste des utilisateurs</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 style="display: inline;" class="m-0 font-weight-bold text-primary">Utilisateurs</h6>
              <span style="float:right">
                <a href="{{route('admin.users.add')}}" class="btn btn-success btn-circle">
                  <i class="fas fa-plus"></i>
                </a>
              </span>
            </div>
            <div class="card-body">
              @if(count($users) > 0)
                <div class="table-responsive">
                  <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                      <tr>
                        <th>Nom - Matricule</th>
                        <th>Courriel</th>
                        <th>est un superviseur</th>
                        <th>Niveau d'accès</th>
                        <th>Actif</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th>Nom - Matricule</th>
                        <th>Courriel</th>
                        <th>est un superviseur</th>
                        <th>Niveau d'accès</th>
                        <th>Actif</th>
                        <th>Actions</th>
                      </tr>
                    </tfoot>
                    <tbody>
                      @foreach($users as $user)
                          <tr>
                            <td>{{$user->name}} - {{$user->number}}</td>
                            <td>{{$user->email}}</td>
                            <td class="text-center">
                              <a href="#" onclick="event.preventDefault(); $('.toggleSupervisorName').html('{{addslashes($user->name)}}'); $('.toggleSupervisorReverseStatus').html('{{($user->is_supervisor ? 'Non' : 'Oui')}}');" data-toggle="modal" data-target="#toggleSupervisorModal">
                                <div class="icon-circle @if($user->is_supervisor) bg-success @else bg-danger @endif">
                                  <i class="text-white far @if($user->is_supervisor) fa-check-circle @else fa-times-circle @endif"></i>
                                </div>
                              </a>
                            </td>
                            <td>{{$user->access_level}}</td>
                            <td class="text-center">
                              <div class="icon-circle @if($user->active) bg-success @else bg-danger @endif">
                                <i class="text-white far @if($user->active) fa-check-circle @else fa-times-circle @endif"></i>
                              </div>
                            </td>
                     
                            <td>
                                <a href="{{route('admin.users.edit', ['user' => $user->id])}}" class="btn btn-warning btn-circle">
                                    <i class="far fa-edit"></i>
                                </a>
                            </td>
                          </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              @else
                <p>Aucun utilisateur dans le système. Vous pouvez utiliser le bouton vert au haut de la page afin d'en créer un nouveau.</p>
              @endif
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->


        <div class="modal fade" id="toggleSupervisorModal" tabindex="-1" role="dialog" aria-labelledby="toggleSupervisorModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="toggleSupervisorModalLabel">Changer le status de superviseur?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">Sélectionnez "Oui" ci-dessous si vous désirez changer le statut de superviseur de l'utilisateur <span class="toggleSupervisorName">#NOM#</span> pour <span class="toggleSupervisorReverseStatus"></span>.</div>
              <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Non</button>
                <a class="btn btn-primary" href="#" onclick="event.preventDefault(); document.getElementById('supervisor-form').submit();">Oui</a>
              </div>
            </div>
          </div>
        </div>

@endsection
