@extends('layouts.base', ['title' => 'Liste des ajouts de ressources en attente'])

@section('content')


<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Liste des ajouts de ressources en attente</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 style="display: inline;" class="m-0 font-weight-bold text-primary">En attente</h6>
              <span style="float:right">
                <a href="{{route('operations.report.ressources')}}" class="btn btn-success btn-circle">
                  <i class="fas fa-plus"></i>
                </a>
              </span>
            </div>
            <div class="card-body">
              @if(count($reports) > 0)
                <div class="table-responsive">
                  <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                      <tr>
                        <th>Type</th>
                        <th>Date de la demande</th>
                        <th>Date de l'ajout</th>
                        <th>Entreprise ambulancière</th>
                        <th>Débuté par</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th>Type</th>
                        <th>Date de la demande</th>
                        <th>Date de l'ajout</th>
                        <th>Entreprise ambulancière</th>
                        <th>Débuté par</th>
                        <th>Actions</th>
                      </tr>
                    </tfoot>
                    <tbody>
                      @foreach($reports as $report)
                          <tr>
                            <td>{{$report->type_to_name()}}</td>
                            <?php Carbon::setLocale('fr_FR'); setlocale(LC_TIME, 'fr_FR');?>
                            <td>{{Carbon::parse($report->request_date)->formatLocalized('%d %B %Y')}}</td>
                            <td>{{Carbon::parse($report->add_date)->formatLocalized('%d %B %Y')}}</td>
                            <td>{{$report->ambulance_company->name}}</td>
                            <td>{{$report->from_rmu->name}}</td>
                            <td>
                                <a href="{{route('operations.report.ressources', ['report' => $report->id])}}" class="btn btn-warning btn-circle">
                                    <i class="fas fa-edit"></i>
                                </a>
                            </td>
                          </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              @else
                <p>Aucun rapport d'ajout de ressources en attente pour le moment. Vous pouvez utiliser le bouton vert au haut de la page afin d'en créer un nouveau.</p>
              @endif
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

@endsection
