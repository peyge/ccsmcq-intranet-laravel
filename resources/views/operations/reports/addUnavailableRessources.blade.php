@extends('layouts.base', ['title' => 'Non-disponibilité des ressources ambulancières'])

@section('content')


<div class="container-fluid">
    <h1 class="h3 mb-4 text-gray-800">Non-disponibilité des ressources ambulancières</h1>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form" role="form" method="POST" action="{{ route('operations.report.unavailableRessources.save', ['report' => $report->id]) }}">
                                {{ csrf_field() }}
                                <p>Inscrire les heures lorsque les TA/P s'annoncent. Si l'équipe ne s'est pas rapportée, cocher la case appropriés.</p>
                                <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                    <label>Type de rapport</label>
                                    <select class="form-control" name="type" id="select-type" required>
                                        <option value="">Veuillez sélectionner...</option>
                                        <option value="full_unavailable" @if(old('type', $report->type) == 'full_unavailable') selected="selected"@endif>10-0-6</option>
                                        <option value="partially_unavailable" @if(old('type', $report->type) == 'partially_unavailable') selected="selected"@endif>10-25</option>
                                    </select>
                                    @if ($errors->has('type'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('type') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group{{ $errors->has('date_from') ? ' has-error' : '' }}">
                                            <label>De</label>
                                            <input class="form-control" type="text" name="date_from" id="date_from" value="{{ old('date_from', $report->date_from) }}">
                                            @if ($errors->has('date_from'))
                                                <span class="help-block border-bottom-danger">
                                                    <strong>{{ $errors->first('date_from') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group{{ $errors->has('date_to') ? ' has-error' : '' }}">
                                            <label>À</label>
                                            <input class="form-control" type="text" name="date_to" id="date_to" value="{{ old('date_to', $report->date_to) }}">
                                            @if ($errors->has('date_to'))
                                                <span class="help-block border-bottom-danger">
                                                    <strong>{{ $errors->first('date_to') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="card mb-4">
                                    <div class="card-header">
                                        Sélectionnez la/les raison(s)
                                    </div>
                                    <div class="card-body">
                                        <div class="for-full_unavailable container-for">
                                            @foreach($reasons as $reason)
                                                <div class="form-group{{ $errors->has('reason_'.$reason->id) ? ' has-error' : '' }}">
                                                    <input type="checkbox" value="{{$reason->id}}"  id="reason_{{$reason->id}}" name="reason[{{$reason->id}}]" {{ old('reason_'.$reason->id, $report->reasons()->where('reason_id', $reason->id)->get()->count()) ? 'checked' : '' }}> 
                                                    <label for="reason_{{$reason->id}}">{{$reason->name}}</label>
                                                    @if ($errors->has('reason_'.$reason->id))
                                                        <span class="help-block border-bottom-danger">
                                                            <strong>{{ $errors->first('reason_'.$reason->id) }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            @endforeach
                                        </div>

                                        <div class="for-partially_unavailable container-for">
                                            @foreach($reasons_partially as $reason)
                                                <div class="form-group{{ $errors->has('reason_'.$reason->id) ? ' has-error' : '' }}">
                                                    <input type="checkbox" value="{{$reason->id}}"  id="reason_{{$reason->id}}" name="reason[{{$reason->id}}]" {{ old('reason_'.$reason->id, $report->reasons()->where('reason_id', $reason->id)->get()->count()) ? 'checked' : '' }}> 
                                                    <label for="reason_{{$reason->id}}">{{$reason->name}}</label>
                                                    @if ($errors->has('reason_'.$reason->id))
                                                        <span class="help-block border-bottom-danger">
                                                            <strong>{{ $errors->first('reason_'.$reason->id) }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="form-group{{ $errors->has('reason_other_chkbx') ? ' has-error' : '' }}">
                                            <input type="checkbox" value="1" class="reason_other_chkbx"  id="reason_other_chkbx" name="reason_other_chkbx" {{ old('reason_other_chkbx', $report->reason_other_chkbx) ? 'checked' : '' }}> 
                                            <label for="reason_other_chkbx">Autre</label>
                                            <span class="reason_other" style="display:none;">
                                                <label>Spécifiez</label>
                                                <input type="text" name="reason_other" id="reason_other" value="{{ old('reason_other', $report->reason_other) }}" />
                                            </span>
                                            @if ($errors->has('reason_-1'))
                                                <span class="help-block border-bottom-danger">
                                                    <strong>{{ $errors->first('reason_-1') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('truck_number') ? ' has-error' : '' }}">
                                    <label>N° du VA</label>
                                    <input type="number" class="form-control" name="truck_number" id="truck_number" value="{{ old('truck_number', $report->truck_number) }}" required />
                                    @if ($errors->has('truck_number'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('truck_number') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('ambulance_company_id') ? ' has-error' : '' }}">
                                    <label>Entreprise ambulancière</label>
                                    <select class="form-control" name="ambulance_company_id" id="ambulance_company_id" required>
                                        <option value="">Veuillez sélectionner...</option>
                                        @foreach($ambulance_companies as $ambulance_company)
                                            <option value="{{$ambulance_company->id}}" @if(old('ambulance_company_id', $report->ambulance_company_id) == $ambulance_company->id) selected="selected"@endif>{{$ambulance_company->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('ambulance_company_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('ambulance_company_id') }}</strong>
                                        </span>
                                    @endif
                                </div>



                                <div class="for-full_unavailable container-for">

                                    <div class="form-group{{ $errors->has('ambulance_company_supervisor_id') ? ' has-error' : '' }}">
                                        <label>Responsable de l'entreprise ambulancière</label>
                                        <select class="form-control" name="ambulance_company_supervisor_id" id="ambulance_company_supervisor_id">
                                            <option value="">Veuillez sélectionner...</option>
                                            @foreach($ambulance_company_supervisors as $ambulance_company_supervisor)
                                                <option class="hideable cie_{{$ambulance_company_supervisor->ambulance_company_id}}" style="display:none;" data-ambulance_company_id="{{$ambulance_company_supervisor->ambulance_company_id}}" value="{{$ambulance_company_supervisor->id}}" @if(old('ambulance_company_supervisor_id', $report->ambulance_company_supervisor_id) == $ambulance_company_supervisor->id) selected="selected"@endif>{{$ambulance_company_supervisor->name}}</option>
                                            @endforeach
                                            <option value="-1" @if(old('ambulance_company_supervisor_id', $report->ambulance_company_supervisor_id) == '-1') selected="selected"@endif>Autre - Veuillez spécifier...</option>
                                        </select>
                                        @if ($errors->has('ambulance_company_supervisor_id'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('ambulance_company_supervisor_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div style="display:none;" class="other-supervisor-container form-group{{ $errors->has('ambulance_company_supervisor_other') ? ' has-error' : '' }}">
                                        <label>Veuillez spécifier</label>
                                        <input type="text" class="form-control" name="ambulance_company_supervisor_other" id="ambulance_company_supervisor_other" value="{{ old('ambulance_company_supervisor_other', $report->ambulance_company_supervisor_other) }}" />
                                        @if ($errors->has('ambulance_company_supervisor_other'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('ambulance_company_supervisor_other') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group{{ $errors->has('additional_truck_to_replace') ? ' has-error' : '' }}">
                                        <p>Y a-t-il une éqipe additionnelle pour remplacer la RA non disponible ?</p>
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <input type="radio" value="1" class="additional_truck_to_replace"  id="additional_truck_to_replace_yes" name="additional_truck_to_replace" {{ old('additional_truck_to_replace', $report->additional_truck_to_replace) == 1 ? 'checked' : '' }}> 
                                                <label for="additional_truck_to_replace_yes">Oui</label>

                                                <input type="radio" value="0" class="additional_truck_to_replace"  id="additional_truck_to_replace_no" name="additional_truck_to_replace" {{ old('additional_truck_to_replace', $report->additional_truck_to_replace) == 0 ? 'checked' : '' }}> 
                                                <label for="additional_truck_to_replace_no">Non</label>
                                            </div>

                                            <div class="col-lg-10 additional_truck_number_container" style="display:none;">
                                                <label>N° du VA</label>
                                                <input type="number" name="additional_truck_number" id="additional_truck_number" value="{{ old('additional_truck_number', $report->additional_truck_number) }}" />
                                                @if ($errors->has('additional_truck_number'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('additional_truck_number') }}</strong>
                                                    </span>
                                                @endif

                                                @if ($errors->has('additional_truck_to_replace'))
                                                    <span class="help-block border-bottom-danger">
                                                        <strong>{{ $errors->first('additional_truck_to_replace') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>



                                    <div class="form-group{{ $errors->has('ccs_supervisor_user_id') ? ' has-error' : '' }}">
                                        <label>Nom du responsable du CCS avisé</label>
                                        <select class="form-control" name="ccs_supervisor_user_id" id="select-ccs_supervisor_user_id">
                                            <option value="">Veuillez sélectionner...</option>
                                            @foreach($ccs_supervisors as $ccs_supervisor)
                                                <option value="{{$ccs_supervisor->id}}" @if(old('ccs_supervisor_user_id', $report->ccs_supervisor_user_id) == $ccs_supervisor->id) selected="selected"@endif>{{$ccs_supervisor->name}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('ccs_supervisor_user_id'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('ccs_supervisor_user_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                </div>

                                


                                <div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
                                    <p>Commentaires</p>
                                    <textarea cols="40" rows="10" name="notes" id="notes">{{ old('notes', $report->notes) }}</textarea>
                                    @if ($errors->has('notes'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('notes') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('from_rmu_user_id') ? ' has-error' : '' }}">
                                    <label>Nom et matricule du RMU</label>
                                    <select class="form-control" name="from_rmu_user_id" id="select-from_rmu_user_id" required>
                                        <option value="">Veuillez sélectionner...</option>
                                        @foreach($users as $ccs_rmu)
                                            <option value="{{$ccs_rmu->id}}" @if(old('from_rmu_user_id', ($report->from_rmu_user_id ? $report->from_rmu_user_id : \Auth::user()->id ) ) == $ccs_rmu->id) selected="selected"@endif>{{$ccs_rmu->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('from_rmu_user_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('from_rmu_user_id') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                




                               @if( $report->is_pending == '1')
                                    <button type="submit" name="is_pending" value="1" class="btn btn-warning btn-icon-split">
                                        <span class="icon text-white-50">
                                            <i class="fas fa-save"></i>
                                        </span>
                                        <span class="text">Enregistrer le brouillon</span>
                                    </button>

                                    <button type="submit" name="is_pending" value="0" class="btn btn-info btn-icon-split">
                                        <span class="icon text-white-50">
                                            <i class="fas fa-paper-plane"></i>
                                        </span>
                                        <span class="text">Envoyer</span>
                                    </button>
                                @else
                                    <button type="submit" name="is_pending" value="0" class="btn btn-info btn-icon-split">
                                        <span class="icon text-white-50">
                                            <i class="fas fa-save"></i>
                                        </span>
                                        <span class="text">Enregistrer</span>
                                    </button>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</div>

@endsection

@push('scripts')
    <script>
        function showHideSubTypes()
        {
            $('.container-for').hide();

            $('.for-'+$('#select-type').val()).show();

            if($('#select-type').val() != '')
            {
                $('.for-all').show();
            }
        }

        function showHideSupervisors()
        {
            $('#ambulance_company_supervisor_id').find('option.hideable').hide();
            $('#ambulance_company_supervisor_id').find('option.hideable').attr('disabled', 'disabled');
            if($('#ambulance_company_id').val() != '')
            {
                $('#ambulance_company_supervisor_id').find('option.cie_'+$('#ambulance_company_id').val()).show();
                $('#ambulance_company_supervisor_id').find('option.cie_'+$('#ambulance_company_id').val()).removeAttr('disabled');
            }
        }

        function showHideOtherSupervisor()
        {
            if($('#ambulance_company_supervisor_id').val() == '-1')
            {
                $('.other-supervisor-container').show();
            }
            else
            {
                $('.other-supervisor-container').hide();
            }
        }

        function showHideOtherReason()
        {
            if($('.reason_other_chkbx').is(':checked'))
            {
                $('.reason_other').show();
            }
            else
            {
                $('.reason_other').hide();
            }
        }

        function showHideAdditionnalTruck()
        {
            if($('#additional_truck_to_replace_yes').is(':checked'))
            {
                $('.additional_truck_number_container').show();
            }
            else
            {
                $('.additional_truck_number_container').hide();
            }
        }

        $(document).ready(function(){
            showHideSubTypes();
            showHideSupervisors();
            showHideOtherSupervisor();
            showHideOtherReason();
            showHideAdditionnalTruck();

            $('#select-type').change(function(e)
            {
                showHideSubTypes();
            });

            $('#ambulance_company_id').change(function(e)
            {
                showHideSupervisors();
            });

            $('#ambulance_company_supervisor_id').change(function(e)
            {
                showHideOtherSupervisor();
            })

            $('.reason_other_chkbx').change(function(e)
            {
                showHideOtherReason();
            });

            $('#date_from').datetimepicker({
                format: 'YYYY-MM-DD HH:mm',
                locale: 'fr-ca'
            });
            $('#date_to').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                format: 'YYYY-MM-DD HH:mm',
                locale: 'fr-ca'
            });

            
            $("#date_from").on("dp.change", function (e) {
                //alert(e.date);
                $('#date_to').data("DateTimePicker").minDate(e.date);
            });
            $("#date_to").on("dp.change", function (e) {
                $('#date_from').data("DateTimePicker").maxDate(e.date);
            });

            $('#additional_truck_to_replace_yes, #additional_truck_to_replace_no').change(function(){
                showHideAdditionnalTruck();
            })
        });
    </script>
@endpush
