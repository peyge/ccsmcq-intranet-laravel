@extends('layouts.base', ['title' => 'Ajout de ressource'])

@section('content')


<div class="container-fluid">
    <h1 class="h3 mb-4 text-gray-800">Ajout de ressource, début hâtif et fin tardive</h1>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form" role="form" method="POST" action="{{ route('operations.report.ressources.save', ['report' => $report->id]) }}">
                                {{ csrf_field() }}
                                <p>Inscrire les heures lorsque les TA/P s'annoncent. Si l'équipe ne s'est pas rapportée, cocher la case appropriés.</p>
                                <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                    <label>Type de rapport</label>
                                    <select class="form-control" name="type" id="select-type" required>
                                        <option value="">Veuillez sélectionner...</option>
                                        <option value="add" @if(old('type', $report->type) == 'add') selected="selected"@endif>Ajout de ressources</option>
                                        <option value="start" @if(old('type', $report->type) == 'start') selected="selected"@endif>Début hâtif</option>
                                        <option value="end" @if(old('type', $report->type) == 'end') selected="selected"@endif>Fin tardive</option>
                                    </select>
                                    @if ($errors->has('type'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('type') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('unfulfilled') ? ' has-error' : '' }} container-for for-all" style="display:none;">
                                    <label for="unfulfilled">Non comblé</label>
                                    <input type="checkbox"  id="unfulfilled" name="unfulfilled" value="1" {{ old('unfulfilled') ? 'checked' : '' }}> 
                                    @if ($errors->has('unfulfilled'))
                                        <span class="help-block border-bottom-danger">
                                            <strong>{{ $errors->first('unfulfilled') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('unfulfilled_reason') ? ' has-error' : '' }} container-for for-reason" style="display:none;">
                                    <label>Raison</label>
                                    <input class="form-control" type="text" name="unfulfilled_reason" id="unfulfilled_reason" value="{{ old('unfulfilled_reason', $report->unfulfilled_reason) }}">
                                    @if ($errors->has('unfulfilled_reason'))
                                        <span class="help-block border-bottom-danger">
                                            <strong>{{ $errors->first('unfulfilled_reason') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="for-add container-for" style="display:none;">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="form-group{{ $errors->has('add1084') ? ' has-error' : '' }}">
                                                <label>10-84</label>
                                                <input class="form-control" type="text" name="add1084" id="add1084" value="{{ old('add1084', $report->add1084) }}">
                                                @if ($errors->has('add1084'))
                                                    <span class="help-block border-bottom-danger">
                                                        <strong>{{ $errors->first('add1084') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <div class="form-group{{ $errors->has('add1086') ? ' has-error' : '' }}">
                                                <label>10-86</label>
                                                <input class="form-control" type="text" name="add1086" id="add1086" value="{{ old('add1086', $report->add1086) }}">
                                                @if ($errors->has('add1086'))
                                                    <span class="help-block border-bottom-danger">
                                                        <strong>{{ $errors->first('add1086') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <div class="form-group{{ $errors->has('add1088') ? ' has-error' : '' }}">
                                                <label>10-88</label>
                                                <input class="form-control" type="text" name="add1088" id="add1088" value="{{ old('add1088', $report->add1088) }}">
                                                @if ($errors->has('add1088'))
                                                    <span class="help-block border-bottom-danger">
                                                        <strong>{{ $errors->first('add1088') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <div class="form-group{{ $errors->has('add1089') ? ' has-error' : '' }}">
                                                <label>10-89</label>
                                                <input class="form-control" type="text" name="add1089" id="add1089" value="{{ old('add1089', $report->add1089) }}">
                                                @if ($errors->has('add1089'))
                                                    <span class="help-block border-bottom-danger">
                                                        <strong>{{ $errors->first('add1089') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="for-start container-for" style="display:none;">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group{{ $errors->has('start1084real') ? ' has-error' : '' }}">
                                                <label>10-84 Réel</label>
                                                <input class="form-control" type="text" name="start1084real" id="start1084real" value="{{ old('start1084real', $report->start1084real) }}">
                                                @if ($errors->has('start1084real'))
                                                    <span class="help-block border-bottom-danger">
                                                        <strong>{{ $errors->first('start1084real') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="form-group{{ $errors->has('start1084planned') ? ' has-error' : '' }}">
                                                <label>10-84 prévu à l'horaire</label>
                                                <input class="form-control" type="text" name="start1084planned" id="start1084planned" value="{{ old('start1084planned', $report->start1084planned) }}">
                                                @if ($errors->has('start1084planned'))
                                                    <span class="help-block border-bottom-danger">
                                                        <strong>{{ $errors->first('start1084planned') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="for-end container-for" style="display:none;">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group{{ $errors->has('end1089real') ? ' has-error' : '' }}">
                                                <label>10-89 Réel</label>
                                                <input class="form-control" type="text" name="end1089real" id="end1089real" value="{{ old('end1089real', $report->end1089real) }}">
                                                @if ($errors->has('end1089real'))
                                                    <span class="help-block border-bottom-danger">
                                                        <strong>{{ $errors->first('end1089real') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="form-group{{ $errors->has('end1089planned') ? ' has-error' : '' }}">
                                                <label>10-89 prévu à l'horaire</label>
                                                <input class="form-control" type="text" name="end1089planned" id="end1089planned" value="{{ old('end1089planned', $report->end1089planned) }}">
                                                @if ($errors->has('end1089planned'))
                                                    <span class="help-block border-bottom-danger">
                                                        <strong>{{ $errors->first('end1089planned') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-lg-6 form-group{{ $errors->has('request_date') ? ' has-error' : '' }}">
                                        <label>Date de la demande</label>
                                        <input type="text" class="form-control" name="request_date" id="request_date" value="{{ old('request_date', ($report->request_date ? $report->request_date : date('Y-m-d'))) }}" required />
                                        @if ($errors->has('request_date'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('request_date') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="col-lg-6 form-group{{ $errors->has('request_time') ? ' has-error' : '' }}">
                                        <label>Heure de la demande</label>
                                        <input type="text" class="form-control" name="request_time" id="request_time" value="{{ old('request_time', ($report->request_time ? $report->request_time : date('H:i'))) }}" required />
                                        @if ($errors->has('request_time'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('request_time') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                </div>

                                <div class="form-group{{ $errors->has('add_date') ? ' has-error' : '' }}">
                                    <label>Date de l'ajout</label>
                                    <input type="text" class="form-control" name="add_date" id="add_date" value="{{ old('add_date', ($report->add_date ? $report->add_date : date('Y-m-d'))) }}" required />
                                    @if ($errors->has('add_date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('add_date') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('truck_number') ? ' has-error' : '' }}">
                                    <label>N° du VA</label>
                                    <input type="number" class="form-control" name="truck_number" id="truck_number" value="{{ old('truck_number', $report->truck_number) }}" required />
                                    @if ($errors->has('truck_number'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('truck_number') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('ambulance_company_id') ? ' has-error' : '' }}">
                                    <label>Entreprise ambulancière</label>
                                    <select class="form-control" name="ambulance_company_id" id="ambulance_company_id" required>
                                        <option value="">Veuillez sélectionner...</option>
                                        @foreach($ambulance_companies as $ambulance_company)
                                            <option value="{{$ambulance_company->id}}" @if(old('ambulance_company_id', $report->ambulance_company_id) == $ambulance_company->id) selected="selected"@endif>{{$ambulance_company->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('ambulance_company_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('ambulance_company_id') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('ambulance_company_supervisor_id') ? ' has-error' : '' }}">
                                    <label>Responsable de l'entreprise ambulancière</label>
                                    <select class="form-control" name="ambulance_company_supervisor_id" id="ambulance_company_supervisor_id" required>
                                        <option value="">Veuillez sélectionner...</option>
                                        @foreach($ambulance_company_supervisors as $ambulance_company_supervisor)
                                            <option class="hideable cie_{{$ambulance_company_supervisor->ambulance_company_id}}" style="display:none;" data-ambulance_company_id="{{$ambulance_company_supervisor->ambulance_company_id}}" value="{{$ambulance_company_supervisor->id}}" @if(old('ambulance_company_supervisor_id', $report->ambulance_company_supervisor_id) == $ambulance_company_supervisor->id) selected="selected"@endif>{{$ambulance_company_supervisor->name}}</option>
                                        @endforeach
                                        <option value="-1" @if(old('ambulance_company_supervisor_id', $report->ambulance_company_supervisor_id) == '-1') selected="selected"@endif>Autre - Veuillez spécifier...</option>
                                    </select>
                                    @if ($errors->has('ambulance_company_supervisor_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('ambulance_company_supervisor_id') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div style="display:none;" class="other-supervisor-container form-group{{ $errors->has('ambulance_company_supervisor_other') ? ' has-error' : '' }}">
                                    <label>Veuillez spécifier</label>
                                    <input type="text" class="form-control" name="ambulance_company_supervisor_other" id="ambulance_company_supervisor_other" value="{{ old('ambulance_company_supervisor_other', $report->ambulance_company_supervisor_other) }}" />
                                    @if ($errors->has('ambulance_company_supervisor_other'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('ambulance_company_supervisor_other') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="card mb-4">
                                    <div class="card-header">
                                        Sélectionnez la/les raison(s)
                                    </div>
                                    <div class="card-body">
                                        @foreach($reasons as $reason)
                                            <div class="form-group{{ $errors->has('reason_'.$reason->id) ? ' has-error' : '' }}">
                                                <input type="checkbox" value="{{$reason->id}}"  id="reason_{{$reason->id}}" name="reason[{{$reason->id}}]" {{ old('reason_'.$reason->id, $report->reasons()->where('reason_id', $reason->id)->get()->count()) ? 'checked' : '' }}> 
                                                <label for="reason_{{$reason->id}}">{{$reason->name}}</label>
                                                @if ($errors->has('reason_'.$reason->id))
                                                    <span class="help-block border-bottom-danger">
                                                        <strong>{{ $errors->first('reason_'.$reason->id) }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        @endforeach
                                        <div class="form-group{{ $errors->has('reason_other_chkbx') ? ' has-error' : '' }}">
                                            <input type="checkbox" value="1" class="reason_other_chkbx"  id="reason_other_chkbx" name="reason_other_chkbx" {{ old('reason_other_chkbx', $report->reason_other_chkbx) ? 'checked' : '' }}> 
                                            <label for="reason_other_chkbx">Autre</label>
                                            <span class="reason_other" style="display:none;">
                                                <label>Spécifiez</label>
                                                <input type="text" name="reason_other" id="reason_other" value="{{ old('reason_other', $report->reason_other) }}" />
                                            </span>
                                            @if ($errors->has('reason_-1'))
                                                <span class="help-block border-bottom-danger">
                                                    <strong>{{ $errors->first('reason_-1') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="card mb-4">
                                    <div class="card-header">
                                        N° carte(s) d'appel(s)
                                    </div>
                                    <div class="card-body">
                                        @foreach($report->cards as $card)
                                            <div class="form-group">
                                                <input type="text" name="card[{{$card->id}}]" value="{{$card->card_number}}" />
                                                <a href="#" class="btn btn-danger btn-circle btn-remove-card-number">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </div>
                                        @endforeach
                                        <a href="#" class="btn btn-success btn-circle btn-add-card-number">
                                            <i class="fas fa-plus"></i>
                                        </a>
                                    </div>
                                </div>


                                <div class="form-group{{ $errors->has('ccs_supervisor_user_id') ? ' has-error' : '' }}">
                                    <label>Nom du responsable du CCS ayant donné l'autorisation</label>
                                    <select class="form-control" name="ccs_supervisor_user_id" id="select-ccs_supervisor_user_id" required>
                                        <option value="">Veuillez sélectionner...</option>
                                        @foreach($ccs_supervisors as $ccs_supervisor)
                                            <option value="{{$ccs_supervisor->id}}" @if(old('ccs_supervisor_user_id', $report->ccs_supervisor_user_id) == $ccs_supervisor->id) selected="selected"@endif>{{$ccs_supervisor->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('ccs_supervisor_user_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('ccs_supervisor_user_id') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('from_rmu_user_id') ? ' has-error' : '' }}">
                                    <label>Nom et matricule du RMU</label>
                                    <select class="form-control" name="from_rmu_user_id" id="select-from_rmu_user_id" required>
                                        <option value="">Veuillez sélectionner...</option>
                                        @foreach($users as $ccs_rmu)
                                            <option value="{{$ccs_rmu->id}}" @if(old('from_rmu_user_id', ($report->from_rmu_user_id ? $report->from_rmu_user_id : \Auth::user()->id ) ) == $ccs_rmu->id) selected="selected"@endif>{{$ccs_rmu->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('from_rmu_user_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('from_rmu_user_id') }}</strong>
                                        </span>
                                    @endif
                                </div>



                                @if( $report->is_pending == '1')
                                    <button type="submit" name="is_pending" value="1" class="btn btn-warning btn-icon-split">
                                        <span class="icon text-white-50">
                                            <i class="fas fa-save"></i>
                                        </span>
                                        <span class="text">Enregistrer le brouillon</span>
                                    </button>

                                    <button type="submit" name="is_pending" value="0" class="btn btn-info btn-icon-split">
                                        <span class="icon text-white-50">
                                            <i class="fas fa-paper-plane"></i>
                                        </span>
                                        <span class="text">Envoyer</span>
                                    </button>
                                @else
                                    <button type="submit" name="is_pending" value="0" class="btn btn-info btn-icon-split">
                                        <span class="icon text-white-50">
                                            <i class="fas fa-save"></i>
                                        </span>
                                        <span class="text">Enregistrer</span>
                                    </button>
                                @endif

                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</div>

@endsection

@push('scripts')
    <script>
        function showHideSubTypes()
        {
            $('.container-for').hide();
                if( ! $('#unfulfilled').is(':checked'))
                {
                    $('.for-'+$('#select-type').val()).show();
                }
                else
                {
                    if($('#select-type').val() != '')
                    {
                        $('.for-reason').show();
                    }
                }
                if($('#select-type').val() != '')
                {
                    $('.for-all').show();
                }
        }

        function showHideSupervisors()
        {
            $('#ambulance_company_supervisor_id').find('option.hideable').hide();
            $('#ambulance_company_supervisor_id').find('option.hideable').attr('disabled', 'disabled');
            if($('#ambulance_company_id').val() != '')
            {
                $('#ambulance_company_supervisor_id').find('option.cie_'+$('#ambulance_company_id').val()).show();
                $('#ambulance_company_supervisor_id').find('option.cie_'+$('#ambulance_company_id').val()).removeAttr('disabled');
            }
        }

        function showHideOtherSupervisor()
        {
            if($('#ambulance_company_supervisor_id').val() == '-1')
            {
                $('.other-supervisor-container').show();
            }
            else
            {
                $('.other-supervisor-container').hide();
            }
        }

        function showHideOtherReason()
        {
            if($('.reason_other_chkbx').is(':checked'))
            {
                $('.reason_other').show();
            }
            else
            {
                $('.reason_other').hide();
            }
        }

        $(document).ready(function(){
            showHideSubTypes();
            showHideSupervisors();
            showHideOtherSupervisor();
            showHideOtherReason();

            $('#select-type').change(function(e)
            {
                showHideSubTypes();
            });

            $('#ambulance_company_id').change(function(e)
            {
                showHideSupervisors();
            });

            $('#ambulance_company_supervisor_id').change(function(e)
            {
                showHideOtherSupervisor();
            })

            $('.reason_other_chkbx').change(function(e)
            {
                showHideOtherReason();
            });

            $('.btn-add-card-number').click(function(e)
            {
                e.preventDefault();

                var input = '<div class="form-group"><input type="text" name="card[]" value="" /></div>';
                $(input).insertBefore('.btn-add-card-number');
            });

            $('.btn-remove-card-number').click(function(e)
            {
                e.preventDefault();
                $(this).closest('.form-group').remove();
            })

            $('#unfulfilled').change(function(e)
            {
                if($(this).is(':checked'))
                {
                    $('.container-for').hide();
                    $('.for-all').show();
                    $('.for-reason').show();
                }
                else
                {
                    $('.container-for').hide();
                    $('.for-'+$('#select-type').val()).show();
                    if($('#select-type').val() != '')
                    {
                        $('.for-all').show();
                    }
                }
            });

            $('#add1084').datetimepicker({
                format: 'YYYY-MM-DD HH:mm',
                locale: 'fr-ca'
            });
            $('#add1086, #add1088, #add1089, #start1084real, #start1084planned, #end1089real, #end1089planned').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                format: 'YYYY-MM-DD HH:mm',
                locale: 'fr-ca'
            });

            $('#request_date').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                format: 'YYYY-MM-DD',
                locale: 'fr-ca',
                maxDate: "{{date('Y-m-d')}}"
            });
            $('#request_time').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                format: 'HH:mm',
                locale: 'fr-ca'
            });
            $('#add_date').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                format: 'YYYY-MM-DD',
                locale: 'fr-ca'
            });
            $("#add1084").on("dp.change", function (e) {
                //alert(e.date);
                $('#add1086').data("DateTimePicker").minDate(e.date);
            });
            $("#add1086").on("dp.change", function (e) {
                $('#add1084').data("DateTimePicker").maxDate(e.date);
                $('#add1088').data("DateTimePicker").minDate(e.date);
            });
            $("#add1088").on("dp.change", function (e) {
                $('#add1086').data("DateTimePicker").maxDate(e.date);
                $('#add1089').data("DateTimePicker").minDate(e.date);
            });
            $("#add1089").on("dp.change", function (e) {
                $('#add1088').data("DateTimePicker").maxDate(e.date);
            });
            $("#start1084real").on("dp.change", function (e) {
                //alert(e.date);
                $('#start1084planned').data("DateTimePicker").minDate(e.date);
            });
            $("#start1084planned").on("dp.change", function (e) {
                $('#start1084real').data("DateTimePicker").maxDate(e.date);
            });
            $("#end1089planned").on("dp.change", function (e) {
                //alert(e.date);
                $('#end1089real').data("DateTimePicker").minDate(e.date);
            });
            $("#end1089real").on("dp.change", function (e) {
                $('#end1089planned').data("DateTimePicker").maxDate(e.date);
            });
        });
    </script>
@endpush
