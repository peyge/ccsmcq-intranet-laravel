@extends('layouts.base', ['title' => 'Mon profil'])

@section('content')


<div class="container-fluid">
    <h1 class="h3 mb-4 text-gray-800">Mon profil</h1>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form" role="form" method="POST" action="{{ route('user.profil.save') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Nom complet</label>
                                    <input class="form-control" type="text" name="name" value="{{ old('name', $user->name) }}" required autofocus>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label>Courriel</label>
                                    <input class="form-control" type="text" name="email" value="{{ old('email', $user->email) }}" required>
                                    <p class="help-block">Courriel utilisé pour la connexion au système et la réception de messages.</p>
                                    @if ($errors->has('email'))
                                        <span class="help-block border-bottom-danger">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Nouveau mot de passe</label>
                                    <input class="form-control" type="password" name="password">
                                    <p class="help-block">Laissez ce champ vide si vous ne désirez pas modifier votre mot de passe.</p>
                                </div>
                                <div class="form-group">
                                    <label>Photo de profil</label>
                                    <input type="file" name="image">
                                </div>

                                <div class="form-group{{ $errors->has('theme') ? ' has-error' : '' }}">
                                    <label>Thème visuel</label>
                                    <select class="form-control" name="theme" id="theme" required>
                                        <option value="">Veuillez sélectionner...</option>
                                        <option value="default" @if(old('theme', $user->theme) == 'default') selected="selected"@endif>Défaut</option>
                                        <option value="monokai" @if(old('theme', $user->theme) == 'monokai') selected="selected"@endif>Monokai</option>
                                    </select>
                                    @if ($errors->has('theme'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('theme') }}</strong>
                                        </span>
                                    @endif
                                </div>


                                <button type="submit" class="btn btn-success btn-icon-split">
                                    <span class="icon text-white-50">
                                        <i class="fas fa-save"></i>
                                    </span>
                                    <span class="text">Enregistrer</span>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</div>

@endsection
