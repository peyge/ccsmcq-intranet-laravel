@extends('layouts.base', ['title' => 'Activités'])

@section('content')


<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Mon activité</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Dernières actions</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Action</th>
                      <th>Détails</th>
                      <th>Date</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Action</th>
                      <th>Détails</th>
                      <th>Date</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($activities as $activity)
                        <tr>
                          <td>{{$activity->description}}</td>
                          <td></td>
                          <?php Carbon::setLocale('fr_FR'); setlocale(LC_TIME, 'fr_FR');?>
                          <td>{{Carbon::parse($activity->created_at)->formatLocalized('%d %B %Y %H:%M:%S')}}</td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

@endsection
